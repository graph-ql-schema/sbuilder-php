<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolverFactoryInterface;

/**
 * Генератор мутации для вставки/обновления сущности
 */
class InsertOrUpdateMutationGenerator extends AbstractQueryGenerator
{
    /** @var string */
    private $operationType;

    /** @var callable */
    private $descriptionGenerator;

    /** @var string */
    private $suffix;

    /** @var ReturningResolverFactoryInterface */
    private $resolverFactory;

    /**
     * InsertOrUpdateMutationGenerator constructor.
     *
     * @param ReturningResolverFactoryInterface $resolverFactory
     * @param AliasGenerationServiceInterface $aliasGenerator
     * @param string $operationType
     * @param callable $descriptionGenerator
     * @param string $suffix
     * @param array $processors
     */
    public function __construct(
        ReturningResolverFactoryInterface $resolverFactory,
        AliasGenerationServiceInterface $aliasGenerator,
        string $operationType,
        callable $descriptionGenerator,
        string $suffix,
        array $processors
    ) {
        parent::__construct($processors, $aliasGenerator);

        $this->operationType = $operationType;
        $this->descriptionGenerator = $descriptionGenerator;
        $this->suffix = $suffix;
        $this->resolverFactory = $resolverFactory;
    }

    /**
     * Получение суффикса для названия мутации/запроса
     *
     * @return string
     */
    public function suffix(): string
    {
        return $this->suffix;
    }

    /**
     * Генерация запроса для сущности
     *
     * @param ObjectType $object
     * @param callable $resolver
     * @return ObjectType|null
     */
    public function generate(ObjectType $object, callable $resolver): ?array
    {
        $name = sprintf('%s_%s', $this->aliasGenerator->generate($object->name), $this->operationType);
        $arguments = $this->generateArguments($object);
        if (null === $arguments) {
            return null;
        }

        $descriptionGenerator = $this->descriptionGenerator;
        return [
            'type' => $this->generateResponse($object, $name),
            'name' => $name,
            'description' => $descriptionGenerator($object),
            'args' => $arguments,
            'resolve' => $resolver,
        ];
    }

    /**
     * Генератор объекта результата обработки запроса
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return ObjectType
     */
    private function generateResponse(ObjectType $object, string $namePrefix): ObjectType {
        return new ObjectType([
            'name' => sprintf("%s_result_type", $namePrefix),
            'fields' => [
                "affected_rows" => [
                    'type' => Type::nonNull(Type::int()),
                    'description' => "Количество записей, обработанных запросом",
                ],
                "returning" => [
                    'type' => Type::listOf($object),
					'description' => "Массив объектов, обработанных в ходе выполнения запроса",
					'resolve' => [$this->resolverFactory->make($object), "parse"],
                ],
            ],
            'description' => sprintf("Результат обработки значений для сущности '%s'", $object->name),
        ]);
    }
}