<?php

namespace SBuilder\QueryStructureGenerator\GroupByParametersGenerator;

use GraphQL\Type\Definition\ObjectType;

/**
 * Generator for sorting parameters.
 */
interface GroupByParametersGeneratorInterface
{
    /**
     * Generating params for sorting.
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, string $namePrefix): array;
}
