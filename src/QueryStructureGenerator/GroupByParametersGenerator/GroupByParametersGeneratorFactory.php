<?php

namespace SBuilder\QueryStructureGenerator\GroupByParametersGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetter;
use Monolog\Logger;

/**
 * @inheritDoc
 */
class GroupByParametersGeneratorFactory implements GroupByParametersGeneratorFactoryInterface
{
    /**
     * @param Logger|null $logger
     * @return GroupByParametersGeneratorInterface
     */
    public static function create(Logger $logger = null): GroupByParametersGeneratorInterface
    {
        return new GroupByParametersGenerator(new GraphQlRootTypeGetter(), $logger);
    }
}
