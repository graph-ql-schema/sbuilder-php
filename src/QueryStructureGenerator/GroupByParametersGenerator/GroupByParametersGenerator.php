<?php

namespace SBuilder\QueryStructureGenerator\GroupByParametersGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use Monolog\Logger;

/**
 * @inheritDoc
 */
class GroupByParametersGenerator implements GroupByParametersGeneratorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var Logger|null */
    private $logger;

    /**
     * @param GraphQlRootTypeGetterInterface $graphQlRootTypeGetter
     * @param Logger|null $logger
     */
    public function __construct(GraphQlRootTypeGetterInterface $graphQlRootTypeGetter, Logger $logger = null)
    {
        $this->typeGetter = $graphQlRootTypeGetter;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function generate(ObjectType $object, string $namePrefix): array
    {
        if ($this->logger) {
            $this->logger->debug(
                sprintf("Started group by parameters generation for object '%s'", $object->name),
                [
                    'object' => $object,
                    'namePrefix' => $namePrefix
                ]
            );
        }

        $name = sprintf('%s_group_by_parameters', $namePrefix);
        return [
            'type' => Type::listOf(new EnumType([
                'name' => sprintf('%s_group_by', $name),
                'description' => 'Field variants for grouping',
                'values' => $this->getEnumConfigMapByObject($object)
            ])),
            'description' => 'Grouping by passed params'
        ];
    }

    /**
     * Generate a collection of values for grouping.
     *
     * @param ObjectType $object
     * @return array
     */
    private function getEnumConfigMapByObject(ObjectType $object): array
    {
        $configMap = [];

        foreach ($object->getFields() as $code => $field) {
            $fieldType = $this->typeGetter->getRootType($field->getType());

            if ($fieldType instanceof ObjectType || $fieldType instanceof UnionType) {
                continue;
            }

            $configMap[$code] = [
                'value' => $code,
                'description' => 'Value of ' . $code
            ];
        }

        return $configMap;
    }
}
