<?php

namespace SBuilder\QueryStructureGenerator\GroupByParametersGenerator;

use Monolog\Logger;

/**
 * Factory for creating GroupByParametersGenerator.
 */
interface GroupByParametersGeneratorFactoryInterface
{
    /**
     * @param Logger|null $logger
     * @return GroupByParametersGeneratorInterface
     */
    public static function create(Logger $logger = null): GroupByParametersGeneratorInterface;
}
