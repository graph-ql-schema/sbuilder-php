<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorInterface;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;

/**
 * Генератор запросов аггрегирующих сущностей для модели
 */
class AggregateQueryGenerator extends AbstractQueryGenerator
{
    /** @var AggregateFieldsGeneratorInterface */
    private $aggregateEntityGenerator;

    /**
     * AggregateQueryGenerator constructor.
     *
     * @param AggregateFieldsGeneratorInterface $aggregateEntityGenerator
     * @param AliasGenerationServiceInterface $aliasGenerator
     * @param array $processors
     */
    public function __construct(
        AggregateFieldsGeneratorInterface $aggregateEntityGenerator,
        AliasGenerationServiceInterface $aliasGenerator,
        array $processors
    ) {
        parent::__construct($processors, $aliasGenerator);
        $this->aggregateEntityGenerator = $aggregateEntityGenerator;
    }

    /**
     * Получение суффикса для названия мутации/запроса
     *
     * @return string
     */
    public function suffix(): string
    {
        return SBuilderConstants::AGGREGATE_QUERY;
    }

    /**
     * Генерация запроса для сущности
     *
     * @param ObjectType $object
     * @param callable $resolver
     * @return ObjectType|null
     */
    public function generate(ObjectType $object, callable $resolver): ?array
    {
        $aggregateObject = $this->aggregateEntityGenerator->generate($object);
        $arguments = $this->generateArguments($object);
        if (null === $arguments) {
            return null;
        }

        return [
            'type' => Type::listOf($aggregateObject),
            'description' => sprintf("Аггрегирующий запрос сущностей '%s'", $object->name),
            'args' => $arguments,
            'resolve' => $resolver,
        ];
    }
}