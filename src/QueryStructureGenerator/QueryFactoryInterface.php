<?php

namespace SBuilder\QueryStructureGenerator;

use Monolog\Logger;

/**
 * Фабрика запросов
 */
interface QueryFactoryInterface
{
    /**
     * Фабрика генератора List запросов
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeListGenerator(?Logger $logger = null): QueryGeneratorInterface;

    /**
     * Фабрика генератора Aggregate запросов
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeAggregateGenerator(?Logger $logger = null): QueryGeneratorInterface;

    /**
     * Фабрика генератора Delete мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeDeleteGenerator(?Logger $logger = null): QueryGeneratorInterface;

    /**
     * Фабрика генератора Insert мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeInsertGenerator(?Logger $logger = null): QueryGeneratorInterface;

    /**
     * Фабрика генератора Update мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeUpdateGenerator(?Logger $logger = null): QueryGeneratorInterface;
}