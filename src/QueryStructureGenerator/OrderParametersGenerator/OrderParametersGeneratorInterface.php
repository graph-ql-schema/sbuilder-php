<?php

namespace SBuilder\QueryStructureGenerator\OrderParametersGenerator;

use GraphQL\Type\Definition\ObjectType;

/**
 * Генератор параметров сортировки
 */
interface OrderParametersGeneratorInterface
{
    /**
     * Генерация параметров сортировки
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, string $namePrefix): array;
}