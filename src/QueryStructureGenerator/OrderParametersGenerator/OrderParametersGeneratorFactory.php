<?php

namespace SBuilder\QueryStructureGenerator\OrderParametersGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetter;
use Monolog\Logger;

/**
 * Фабрика сервиса
 */
class OrderParametersGeneratorFactory implements OrderParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return OrderParametersGeneratorInterface
     */
    public static function make(?Logger $logger): OrderParametersGeneratorInterface
    {
        return new OrderParametersGenerator(
            new GraphQlRootTypeGetter(),
            $logger
        );
    }
}