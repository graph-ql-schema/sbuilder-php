<?php

namespace SBuilder\QueryStructureGenerator\OrderParametersGenerator;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface OrderParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return OrderParametersGeneratorInterface
     */
    public static function make(?Logger $logger): OrderParametersGeneratorInterface;
}