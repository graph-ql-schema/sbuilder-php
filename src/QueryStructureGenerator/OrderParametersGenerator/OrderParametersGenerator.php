<?php

namespace SBuilder\QueryStructureGenerator\OrderParametersGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use Monolog\Logger;

/**
 * Генератор параметров сортировки
 */
class OrderParametersGenerator implements OrderParametersGeneratorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var Logger|null */
    private $logger;

    /**
     * OrderParametersGenerator constructor.
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter, ?Logger $logger)
    {
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
    }

    /**
     * Генерация параметров сортировки
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, string $namePrefix): array
    {
        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Started order parameters generation for object '%s'", $object->name),
                [
                    "object" => $object,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        $name = sprintf("%s_order_parameters", $namePrefix);
        return [
            'type' => Type::listOf(new InputObjectType([
                'name' => sprintf("%s_object", $name),
                'fields' => [
                    "by" => [
                        'type' => new EnumType([
                            'name' => sprintf("%s_object_by", $name),
                            'description' => "Варианты выбора полей для сортировки",
                            'values' => $this->getEnumVariantsByObject($object),
                        ]),
                        'description' => "Варианты выбора полей для сортировки",
                    ],
                    "direction" => [
                        'type' => new EnumType([
                            'name' => sprintf("%s_object_order", $name),
                            'description' => "Варианты сортировки",
                            'values' => [
                                "asc" => [
                                    'value' => "asc",
                                    'description' => "Сортировка по возрастанию"
                                ],
                                "desc" => [
                                    'value' => "desc",
                                    'description' => "Сортировка по убыванию"
                                ],
                            ],
                        ]),
                    ],
                    "priority" => [
                        'type' => Type::int(),
                        'defaultValue' => 500,
                        'description' => "Приоритет применения сортировки. Должен быть больше 0.",
                    ],
                ],
                "description" => "Сортировка по переданным параметрам",
            ])),
            "description" => "Сортировка по переданным параметрам",
        ];
    }

    /**
     * Генерация коллекции значений для сортировки
     *
     * @param ObjectType $object
     * @return array
     */
    private function getEnumVariantsByObject(ObjectType $object): array {
	    $configMap = [];
	    foreach ($object->getFields() as $code => $field) {
	        $fieldType = $this->typeGetter->getRootType($field->getType());
	        if ($fieldType instanceof ObjectType || $fieldType instanceof UnionType) {
	            continue;
            }

	        $configMap[$code] = [
                'value' => $code,
                'description' => sprintf("Значение %s", $code)
            ];
        }

	    return $configMap;
    }
}
