<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;

/**
 * Генератор запроса для сущности
 */
interface QueryGeneratorInterface
{
    /**
     * Получение суффикса для названия мутации/запроса
     *
     * @return string
     */
    public function suffix(): string;

    /**
     * Генерация запроса для сущности
     *
     * @param ObjectType $object
     * @param callable $resolver
     * @return array|null
     */
    public function generate(ObjectType $object, callable $resolver): ?array;
}