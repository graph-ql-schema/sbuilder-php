<?php

namespace SBuilder\QueryStructureGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetter;
use GraphQL\Type\Definition\ObjectType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorFactory;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceFactory;
use SBuilder\QueryStructureGenerator\GenerationProcessors\GroupByGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\LimitQueryGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\ObjectInsertGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\OffsetQueryGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\OrderGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\SetObjectGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GenerationProcessors\WhereOrHavingQueryGeneratorProcessor;
use SBuilder\QueryStructureGenerator\GroupByParametersGenerator\GroupByParametersGeneratorFactory;
use SBuilder\QueryStructureGenerator\OrderParametersGenerator\OrderParametersGeneratorFactory;
use SBuilder\QueryStructureGenerator\PaginationParametersGenerator\PaginationParametersGeneratorFactory;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolverFactory;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersGeneratorFactory;

/**
 * Фабрика запросов
 */
class QueryFactory implements QueryFactoryInterface
{
    /**
     * Фабрика генератора List запросов
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeListGenerator(?Logger $logger = null): QueryGeneratorInterface
    {
        return new ListQueryGenerator(
            [
                new WhereOrHavingQueryGeneratorProcessor(
                    WhereParametersGeneratorFactory::make($logger),
                    SBuilderConstants::WHERE_SCHEMA_KEY
                ),
                new LimitQueryGeneratorProcessor(PaginationParametersGeneratorFactory::make()),
                new OffsetQueryGeneratorProcessor(PaginationParametersGeneratorFactory::make()),
                new OrderGeneratorProcessor(OrderParametersGeneratorFactory::make($logger)),
            ],
            AliasGenerationServiceFactory::create()
        );
    }

    /**
     * Фабрика генератора Aggregate запросов
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeAggregateGenerator(?Logger $logger = null): QueryGeneratorInterface
    {
        return new AggregateQueryGenerator(
            AggregateFieldsGeneratorFactory::make($logger),
            AliasGenerationServiceFactory::create(),
            [
                new GroupByGeneratorProcessor(GroupByParametersGeneratorFactory::create($logger)),
                new WhereOrHavingQueryGeneratorProcessor(
                    WhereParametersGeneratorFactory::make($logger),
                    SBuilderConstants::HAVING_SCHEMA_KEY
                ),
                new WhereOrHavingQueryGeneratorProcessor(
                    WhereParametersGeneratorFactory::make($logger),
                    SBuilderConstants::WHERE_SCHEMA_KEY
                ),
                new LimitQueryGeneratorProcessor(PaginationParametersGeneratorFactory::make()),
                new OffsetQueryGeneratorProcessor(PaginationParametersGeneratorFactory::make()),
            ]
        );
    }

    /**
     * Фабрика генератора Delete мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeDeleteGenerator(?Logger $logger = null): QueryGeneratorInterface
    {
        return new DeleteMutationGenerator(
            [
                new WhereOrHavingQueryGeneratorProcessor(
                    WhereParametersGeneratorFactory::make($logger),
                    SBuilderConstants::WHERE_SCHEMA_KEY
                ),
            ],
            AliasGenerationServiceFactory::create()
        );
    }

    /**
     * Фабрика генератора Insert мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeInsertGenerator(?Logger $logger = null): QueryGeneratorInterface
    {
        return new InsertOrUpdateMutationGenerator(
            new ReturningResolverFactory(),
            AliasGenerationServiceFactory::create(),
            "insert",
            function (ObjectType $object): string {
                return sprintf("Мутация вставки значений объекта '%s'", $object->name);
            },
            SBuilderConstants::INSERT_MUTATION,
            [
                new ObjectInsertGeneratorProcessor(
                    new GraphQlRootTypeGetter(),
                    AliasGenerationServiceFactory::create()
                ),
            ]
        );
    }

    /**
     * Фабрика генератора Update мутаций
     *
     * @param Logger|null $logger
     * @return QueryGeneratorInterface
     */
    public static function makeUpdateGenerator(?Logger $logger = null): QueryGeneratorInterface
    {
        return new InsertOrUpdateMutationGenerator(
            new ReturningResolverFactory(),
            AliasGenerationServiceFactory::create(),
            "update",
            function (ObjectType $object): string {
                return sprintf("Мутация обновления значений объекта '%s'", $object->name);
            },
            SBuilderConstants::UPDATE_MUTATION,
            [
                new SetObjectGeneratorProcessor(
                    new GraphQlRootTypeGetter(),
                    AliasGenerationServiceFactory::create()
                ),
                new WhereOrHavingQueryGeneratorProcessor(
                    WhereParametersGeneratorFactory::make($logger),
                    SBuilderConstants::WHERE_SCHEMA_KEY
                ),
            ]
        );
    }
}