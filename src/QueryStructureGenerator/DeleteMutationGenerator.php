<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SBuilder\Constants\SBuilderConstants;

/**
 * Генератор мутации удаления сущности
 */
class DeleteMutationGenerator extends AbstractQueryGenerator
{
    /**
     * Получение суффикса для названия мутации/запроса
     *
     * @return string
     */
    public function suffix(): string
    {
        return SBuilderConstants::DELETE_MUTATION;
    }

    /**
     * Генерация запроса для сущности
     *
     * @param ObjectType $object
     * @param callable $resolver
     * @return ObjectType|null
     */
    public function generate(ObjectType $object, callable $resolver): ?array
    {
        $name = sprintf('%s_delete', $this->aliasGenerator->generate($object->name));
        $arguments = $this->generateArguments($object);
        if (null === $arguments) {
            return null;
        }

        return [
            'type' => $this->generateResponse($object, $name),
            'name' => $name,
            'description' => sprintf("Мутация удаления значений объекта '%s'", $object->name),
            'args' => $arguments,
            'resolve' => $resolver,
        ];
    }

    /**
     * Генератор объекта результата обработки запроса
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return ObjectType
     */
    private function generateResponse(ObjectType $object, string $namePrefix): ObjectType {
        return new ObjectType([
            'name' => sprintf("%s_result_type", $namePrefix),
            'fields' => [
                "affected_rows" => [
                    'type' => Type::nonNull(Type::int()),
                    'description' => "Количество записей, удаленных запросом",
                ]
            ],
            'description' => sprintf("Результат удаления значений для сущности '%s'", $object->name),
        ]);
    }
}