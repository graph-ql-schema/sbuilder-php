<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use SBuilder\Constants\SBuilderConstants;

/**
 * Генератор запросов листинга сущностей
 */
class ListQueryGenerator extends AbstractQueryGenerator
{
    /**
     * Получение суффикса для названия мутации/запроса
     *
     * @return string
     */
    public function suffix(): string
    {
        return SBuilderConstants::LIST_QUERY;
    }

    /**
     * Генерация запроса для сущности
     *
     * @param ObjectType $object
     * @param callable $resolver
     * @return ObjectType|null
     */
    public function generate(ObjectType $object, callable $resolver): ?array
    {
        $arguments = $this->generateArguments($object);
        if (null === $arguments) {
            return null;
        }

        return [
            'type' => Type::listOf($object),
            'name' => sprintf("%s%s", $this->aliasGenerator->generate($object->name), $this->suffix()),
            'description' => sprintf("Листинг сущностей '%s'", $object->name),
            'args' => $arguments,
            'resolve' => $resolver,
        ];
    }
}