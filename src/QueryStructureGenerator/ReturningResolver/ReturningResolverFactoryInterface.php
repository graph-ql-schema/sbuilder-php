<?php

namespace SBuilder\QueryStructureGenerator\ReturningResolver;

use GraphQL\Type\Definition\ObjectType;

/**
 * Фабрика сервиса
 */
interface ReturningResolverFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param ObjectType $objectType
     * @return ReturningResolverInterface
     */
    public static function make(ObjectType $objectType): ReturningResolverInterface;
}