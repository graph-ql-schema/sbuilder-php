<?php

namespace SBuilder\QueryStructureGenerator\ReturningResolver;

use GqlSqlConverter\Converter\GraphQlSqlConverterInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;
use LogicException;
use Throwable;

/**
 * Резолвер поля returning для обновления/создания сущности
 */
class ReturningResolver implements ReturningResolverInterface
{
    /** @var GraphQlSqlConverterInterface */
    protected $valueConverter;

    /** @var ObjectType */
    protected $object;

    /**
     * ReturningResolver constructor.
     *
     * @param GraphQlSqlConverterInterface $valueConverter
     * @param ObjectType $object
     */
    public function __construct(GraphQlSqlConverterInterface $valueConverter, ObjectType $object)
    {
        $this->valueConverter = $valueConverter;
        $this->object = $object;
    }

    /**
     * Резолвинг поля returning для обновления/создания сущности
     *
     * @param $parentData
     * @return mixed
     * @throws LogicException
     */
    public function parse($parentData)
    {
        if (null === $parentData) {
            return null;
        }

        try {
            $data = json_decode(json_encode($parentData), true);
        } catch (Throwable $exception) {
            throw new LogicException("Failed to generate response for returning field", 500, $exception);
        }

        if (!is_array($data)) {
            throw new LogicException("Passed data is not completely contains required data");
        }

        if (!array_key_exists("returning", $data)) {
            return null;
        }

        if (!is_array($data["returning"])) {
            return $data["returning"];
        }

        $result = [];
        foreach ($data["returning"] as $item) {
            if (!is_array($item)) {
                throw new LogicException("Returning value is incompatible. Array of items expected.", 500);
            }

            try {
                $convertedItem = [];
                foreach ($item as $fieldCode => $value) {
                    $convertedItem[$fieldCode] = $this->convertValueToGraphQL($fieldCode, $value);
                }
            } catch (Throwable $exception) {
                throw new LogicException(sprintf("Failed to convert value for field %s", $fieldCode), 500, $exception);
            }

            $result[] = $convertedItem;
        }

        return $result;
    }

    /**
     * Конвертация переданного значения в валидный для GraphQL формат.
     *
     * @param string $fieldCode
     * @param $value
     *
     * @return array|mixed
     * @throws ConvertationException
     */
    private function convertValueToGraphQL(string $fieldCode, $value) {
        if (!is_array($value)) {
            return $this->valueConverter->toBaseType($this->object, $fieldCode, $value);
        }

        $result = [];
        foreach ($value as $key => $val) {
            $result[$key] = $this->valueConverter->toBaseType($this->object, $fieldCode, $val);
        }

        return $result;
    }
}