<?php

namespace SBuilder\QueryStructureGenerator\ReturningResolver;

use LogicException;

/**
 * Резолвер поля returning для обновления/создания сущности
 */
interface ReturningResolverInterface
{
    /**
     * Резолвинг поля returning для обновления/создания сущности
     *
     * @param $parentData
     * @return mixed
     * @throws LogicException
     */
    public function parse($parentData);
}