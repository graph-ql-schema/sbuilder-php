<?php

namespace SBuilder\QueryStructureGenerator\ReturningResolver;

use GqlSqlConverter\GraphQlSqlConverterFactory;
use GraphQL\Type\Definition\ObjectType;

/**
 * Фабрика сервиса
 */
class ReturningResolverFactory implements ReturningResolverFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param ObjectType $objectType
     * @return ReturningResolverInterface
     */
    public static function make(ObjectType $objectType): ReturningResolverInterface
    {
        return new ReturningResolver(GraphQlSqlConverterFactory::make(), $objectType);
    }
}