<?php

namespace SBuilder\QueryStructureGenerator\PaginationParametersGenerator;

/**
 * Фабрика сервиса
 */
class PaginationParametersGeneratorFactory implements PaginationParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @return PaginationParametersGeneratorInterface
     */
    public static function make(): PaginationParametersGeneratorInterface
    {
        return new PaginationParametersGenerator();
    }
}