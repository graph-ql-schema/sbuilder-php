<?php

namespace SBuilder\QueryStructureGenerator\PaginationParametersGenerator;

/**
 * Фабрика сервиса
 */
interface PaginationParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @return PaginationParametersGeneratorInterface
     */
    public static function make(): PaginationParametersGeneratorInterface;
}