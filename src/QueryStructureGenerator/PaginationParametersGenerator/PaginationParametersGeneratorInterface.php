<?php

namespace SBuilder\QueryStructureGenerator\PaginationParametersGenerator;

/**
 * Генератор параметров пагинации
 */
interface PaginationParametersGeneratorInterface
{
    /**
     * Генерация параметров "Limit"
     *
     * @return array
     */
    public function generateLimit(): array;

    /**
     * Генерация параметров "Offset"
     *
     * @return array
     */
    public function generateOffset(): array;
}