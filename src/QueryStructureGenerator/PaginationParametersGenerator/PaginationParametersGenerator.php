<?php

namespace SBuilder\QueryStructureGenerator\PaginationParametersGenerator;

use GraphQL\Type\Definition\Type;

/**
 * Генератор параметров пагинации
 */
class PaginationParametersGenerator implements PaginationParametersGeneratorInterface
{
    /**
     * Генерация параметров "Limit"
     *
     * @return array
     */
    public function generateLimit(): array
    {
        return [
            'type' => Type::int(),
		    'defaultValue' => 30,
		    'description' => "Параметр пагинации: Количество элементов в выдаче.",
        ];
    }

    /**
     * Генерация параметров "Offset"
     *
     * @return array
     */
    public function generateOffset(): array
    {
        return [
            'type' => Type::int(),
            'defaultValue' => 0,
            'description' => "Параметр пагинации: Смещение для элементов в выдаче.",
        ];
    }
}