<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface WhereParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return WhereParametersGeneratorInterface
     */
    public static function make(?Logger $logger): WhereParametersGeneratorInterface;
}