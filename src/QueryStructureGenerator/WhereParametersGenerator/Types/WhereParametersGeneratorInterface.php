<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types;

use GraphQL\Type\Definition\ObjectType;

/**
 * Генератор параметров Where для запросов GraphQL
 */
interface WhereParametersGeneratorInterface
{
    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array|null
     */
    public function buildParameters(ObjectType $object, string $namePrefix, int $level): ?array;
}