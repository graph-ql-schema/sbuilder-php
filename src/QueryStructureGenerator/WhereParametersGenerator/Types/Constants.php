<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types;

/**
 * Константы сервиса
 */
class Constants
{
    const MAX_DEPTH_LEVEL_FOR_RELATIONS = 3;
}