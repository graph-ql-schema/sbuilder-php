<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQlNullableField\Nullable;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;

/**
 * Процессор генерации оператора "_in"
 */
class InWhereParametersOperatorGeneratorProcessor extends AbstractWhereParametersOperatorGeneratorProcessor
{
    /**
     * InWhereParametersOperatorGeneratorProcessor constructor.
     *
     * @param string[] $availableTypes
     * @param int $maxLevel
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(
        array $availableTypes,
        int $maxLevel,
        GraphQlRootTypeGetterInterface $typeGetter,
        ?Logger $logger
    ) {
        parent::__construct(
            $availableTypes,
            $maxLevel,
            SBuilderConstants::IN_SCHEMA_KEY,
            $typeGetter,
            $logger
        );
    }

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array
    {
        $rType = $this->typeGetter->getRootType($field->getType());
        if ($rType instanceof ScalarType && $this->typeGetter->isNullable($field->getType())) {
            $rType = Nullable::create($rType);
        }

        if (null !== $this->logger) {
            $this->logger->debug(
                "Generated _IN operator for field",
                [
                    "field" => $field,
                    "level" => $level,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        return [
            'type' => Type::listOf($rType),
            'description' => "Операция множественного сравнения значения поля с переданным",
        ];
    }
}