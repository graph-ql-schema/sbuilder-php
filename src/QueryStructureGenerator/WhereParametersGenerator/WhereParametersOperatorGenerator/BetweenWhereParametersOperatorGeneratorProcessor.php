<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\InputObjectType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;

/**
 * Процессор генерации оператора "_between"
 */
class BetweenWhereParametersOperatorGeneratorProcessor extends AbstractWhereParametersOperatorGeneratorProcessor
{
    /**
     * BetweenWhereParametersOperatorGeneratorProcessor constructor.
     *
     * @param string[] $availableTypes
     * @param int $maxLevel
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(
        array $availableTypes,
        int $maxLevel,
        GraphQlRootTypeGetterInterface $typeGetter,
        ?Logger $logger
    ) {
        parent::__construct(
            $availableTypes,
            $maxLevel,
            SBuilderConstants::BETWEEN_SCHEMA_KEY,
            $typeGetter,
            $logger
        );
    }

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array
    {
        $rType = $this->typeGetter->getRootType($field->getType());

        if (null !== $this->logger) {
            $this->logger->debug(
                "Generated _IN operator for field",
                [
                    "field" => $field,
                    "level" => $level,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        return [
            'type' => new InputObjectType([
                'name' => sprintf("%s_between", $namePrefix),
                'fields' => [
                    '_from' => [
                        'type' => $rType,
                        'description' => "Параметр от",
                    ],
                    '_to' => [
                        'type' => $rType,
                        'description' => "Параметр до",
                    ],
                ],
                'description' => ''
            ]),
            'description' => "Операция сравнения значения в формате: 'между двух значений'",
        ];
    }
}