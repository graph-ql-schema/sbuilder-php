<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\UnionType;
use Monolog\Logger;

/**
 * Генератор операторов поля для Where
 */
class WhereParametersOperatorGenerator implements WhereParametersOperatorGeneratorInterface
{
    /** @var WhereParametersOperatorGeneratorProcessorInterface[] */
    private $processors;

    /** @var GraphQlRootTypeGetterInterface */
    protected $typeGetter;

    /** @var Logger|null */
    protected $logger;

    /**
     * WhereParametersOperatorGenerator constructor.
     *
     * @param WhereParametersOperatorGeneratorProcessorInterface[] $processors
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(array $processors, GraphQlRootTypeGetterInterface $typeGetter, ?Logger $logger)
    {
        $this->processors = $processors;
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array
     */
    public function generateOperations(ObjectType $object, string $namePrefix, int $level): ?array
    {
        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Generation operations for object. Code: %s", $namePrefix),
                [
                    "object" => $object,
                    "level" => $level,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        $result = [];
        foreach ($object->getFields() as $name => $field) {
            if (null !== $this->logger) {
                $this->logger->debug(sprintf("Generation configuration for field %s", $name));
            }

            $fieldType = $this->typeGetter->getRootType($field->getType());
            if ($fieldType instanceof ObjectType || $fieldType instanceof UnionType) {
                continue;
            }


            $fieldName = sprintf("%s_%s", $namePrefix, str_replace(" ", "_", strtolower($field->name)));
	        $fieldDescription = sprintf("Фильтр по полю '%s' для сущности", $field->name);

            $fieldsConfig = [];
            foreach ($this->processors as $processor) {
                if (!$processor->isAvailable($field, $level)) {
                    continue;
                }

                $config = $processor->generate($field, $level, sprintf("%s_operation", $fieldName));
                if (0 === count($config)) {
                    continue;
                }

                $fieldsConfig[$processor->operatorCode()] = $config;
            }

            if (0 === count($fieldsConfig)) {
                continue;
            }

            $result[$name] = [
                'type' => new InputObjectType([
                    'name' => $fieldName,
                    'fields' => $fieldsConfig,
                    'description' => $fieldDescription,
                ]),
                'description' => $fieldDescription,
            ];
        }

        if (0 === count($result)) {
            return null;
        }

        return $result;
    }
}
