<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\ObjectType;

/**
 * Генератор операторов поля для Where
 */
interface WhereParametersOperatorGeneratorInterface
{
    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array
     */
    public function generateOperations(ObjectType $object, string $namePrefix, int $level): ?array;
}