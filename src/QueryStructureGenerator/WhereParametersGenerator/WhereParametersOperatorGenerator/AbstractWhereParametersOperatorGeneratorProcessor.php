<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;


use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use Monolog\Logger;

/**
 * Абстрактный класс процессора, реализующий общий функционал
 */
abstract class AbstractWhereParametersOperatorGeneratorProcessor implements WhereParametersOperatorGeneratorProcessorInterface
{
    /** @var string[] */
    protected $availableTypes;

    /** @var int */
    protected $maxLevel;

    /** @var GraphQlRootTypeGetterInterface */
    protected $typeGetter;

    /** @var Logger|null */
    protected $logger;

    /** @var string */
    protected $operationCode;

    /**
     * AbstractWhereParametersOperatorGeneratorProcessor constructor.
     *
     * @param string[] $availableTypes
     * @param int $maxLevel
     * @param string $operationCode
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(
        array $availableTypes,
        int $maxLevel,
        string $operationCode,
        GraphQlRootTypeGetterInterface $typeGetter,
        ?Logger $logger
    ) {
        $this->availableTypes = $availableTypes;
        $this->maxLevel = $maxLevel;
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
        $this->operationCode = $operationCode;
    }

    /**
     * Проверка доступности процессора
     *
     * @param FieldDefinition $field
     * @param int $level
     * @return bool
     */
    public function isAvailable(FieldDefinition $field, int $level): bool
    {
        if ($level > $this->maxLevel) {
            return false;
        }

        $fieldRootType = $this->typeGetter->getRootType($field->getType());

        return in_array(get_class($fieldRootType), $this->availableTypes);
    }

    /**
     * Код оператора, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operatorCode(): string {
        return $this->operationCode;
    }

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    abstract public function generate(FieldDefinition $field, int $level, string $namePrefix): array;
}