<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;

/**
 * Обработчик генератора операторов поля для Where
 */
interface WhereParametersOperatorGeneratorProcessorInterface
{
    /**
     * Проверка доступности процессора
     *
     * @param FieldDefinition $field
     * @param int $level
     * @return bool
     */
    public function isAvailable(FieldDefinition $field, int $level): bool;

    /**
     * Код оператора, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operatorCode(): string;

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array;
}