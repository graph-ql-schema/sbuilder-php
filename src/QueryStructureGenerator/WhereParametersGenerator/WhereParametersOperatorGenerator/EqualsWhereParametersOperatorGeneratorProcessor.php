<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\ScalarType;
use GraphQlNullableField\Nullable;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;

/**
 * Процессор генерации оператора "_equals"
 */
class EqualsWhereParametersOperatorGeneratorProcessor implements WhereParametersOperatorGeneratorProcessorInterface
{
    /** @var int */
    protected $maxLevel;

    /** @var GraphQlRootTypeGetterInterface */
    protected $typeGetter;

    /** @var Logger|null */
    protected $logger;

    /**
     * EqualsWhereParametersOperatorGeneratorProcessor constructor.
     *
     * @param int $maxLevel
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(int $maxLevel, GraphQlRootTypeGetterInterface $typeGetter, ?Logger $logger)
    {
        $this->maxLevel = $maxLevel;
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
    }

    /**
     * Проверка доступности процессора
     *
     * @param FieldDefinition $field
     * @param int $level
     * @return bool
     */
    public function isAvailable(FieldDefinition $field, int $level): bool
    {
        return $level <= $this->maxLevel;
    }

    /**
     * Код оператора, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operatorCode(): string
    {
        return SBuilderConstants::EQUALS_SCHEMA_KEY;
    }

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array
    {
        $rType = $this->typeGetter->getRootType($field->getType());
        if ($rType instanceof ScalarType && $this->typeGetter->isNullable($field->getType())) {
            $rType = Nullable::create($rType);
        }

        if (null !== $this->logger) {
            $this->logger->debug(
                "Generated _EQUALS operator for field",
                [
                    "field" => $field,
                    "level" => $level,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        return [
            'type' => $rType,
            'description' => "Операция строго сравнения значения поля с переданным",
        ];
    }
}