<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;

/**
 * Процессор генерации операторов с ограниченным набором доступных типов
 */
class ConfigurableWhereParametersOperatorGeneratorProcessor extends AbstractWhereParametersOperatorGeneratorProcessor
{
    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array
    {
        $rType = $this->typeGetter->getRootType($field->getType());

        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Generated %s operator for field", $this->operationCode),
                [
                    "field" => $field,
                    "level" => $level,
                    "namePrefix" => $namePrefix,
                ]
            );
        }

        return [
            'type' => $rType,
		    'description' => sprintf("Операция %s со значением поля", $this->operationCode),
        ];
    }
}