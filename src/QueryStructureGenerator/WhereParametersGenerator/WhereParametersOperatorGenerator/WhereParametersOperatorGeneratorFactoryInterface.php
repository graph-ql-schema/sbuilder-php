<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface WhereParametersOperatorGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return WhereParametersOperatorGeneratorInterface
     */
    public static function make(?Logger $logger): WhereParametersOperatorGeneratorInterface;
}