<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GqlDatetime\Scalars\DateScalarType;
use GqlDatetime\Scalars\DateTimeScalarType;
use GqlDatetime\Scalars\TimeScalarType;
use GqlRootTypeGetter\GraphQlRootTypeGetter;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\IDType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\StringType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\Constants;

/**
 * Фабрика сервиса
 */
class WhereParametersOperatorGeneratorFactory implements WhereParametersOperatorGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return WhereParametersOperatorGeneratorInterface
     */
    public static function make(?Logger $logger): WhereParametersOperatorGeneratorInterface
    {
        $numericTypes = [
            IntType::class,
            FloatType::class,
            DateScalarType::class,
            TimeScalarType::class,
            DateTimeScalarType::class,
        ];

        $typeGetter = new GraphQlRootTypeGetter();
        $processors = [
            new EqualsWhereParametersOperatorGeneratorProcessor(Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS, $typeGetter, $logger),
            new ConfigurableWhereParametersOperatorGeneratorProcessor(
                $numericTypes,
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                SBuilderConstants::MORE_SCHEMA_KEY,
                $typeGetter,
                $logger
            ),
            new ConfigurableWhereParametersOperatorGeneratorProcessor(
                $numericTypes,
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                SBuilderConstants::LESS_SCHEMA_KEY,
                $typeGetter,
                $logger
            ),
            new ConfigurableWhereParametersOperatorGeneratorProcessor(
                $numericTypes,
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                SBuilderConstants::MORE_OR_EQUALS_SCHEMA_KEY,
                $typeGetter,
                $logger
            ),
            new ConfigurableWhereParametersOperatorGeneratorProcessor(
                $numericTypes,
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                SBuilderConstants::LESS_OR_EQUALS_SCHEMA_KEY,
                $typeGetter,
                $logger
            ),
            new ConfigurableWhereParametersOperatorGeneratorProcessor(
                [StringType::class],
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                SBuilderConstants::LIKE_SCHEMA_KEY,
                $typeGetter,
                $logger
            ),
            new InWhereParametersOperatorGeneratorProcessor(
                array_merge([
                    IDType::class,
                    StringType::class,
                ], $numericTypes),
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                $typeGetter,
                $logger
            ),
            new BetweenWhereParametersOperatorGeneratorProcessor(
                $numericTypes,
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                $typeGetter,
                $logger
            )
        ];

        return new WhereParametersOperatorGenerator($processors, $typeGetter, $logger);
    }
}