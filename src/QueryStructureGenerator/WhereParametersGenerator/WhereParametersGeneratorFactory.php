<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator;

use Monolog\Logger;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorFactoryInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorFactory;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorFactory;

/**
 * Фабрика сервиса
 */
class WhereParametersGeneratorFactory implements WhereParametersGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return WhereParametersGeneratorInterface
     */
    public static function make(?Logger $logger): WhereParametersGeneratorInterface
    {
        $operatorGenerator = WhereParametersOperatorGeneratorFactory::make($logger);
        $logicOperationsGenerator = WhereParametersLogicOperationsGeneratorFactory::make(
            $operatorGenerator,
            $logger
        );

        $mainGenerator = new WhereParametersGenerator($logicOperationsGenerator, $operatorGenerator, $logger);
        $logicOperationsGenerator->setWhereParametersOperatorGenerator($mainGenerator);

        return $mainGenerator;
    }
}