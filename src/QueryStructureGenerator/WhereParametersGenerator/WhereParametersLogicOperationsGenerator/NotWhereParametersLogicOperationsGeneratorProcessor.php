<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

/**
 * Процессор генератора оператора "not"
 */
class NotWhereParametersLogicOperationsGeneratorProcessor implements WhereParametersLogicOperationsGeneratorProcessorInterface
{
    /** @var WhereParametersOperatorGeneratorInterface */
    private $generator;

    /** @var Logger|null */
    private $logger;

    /**
     * NotWhereParametersLogicOperationsGeneratorProcessor constructor.
     * @param WhereParametersOperatorGeneratorInterface $generator
     * @param Logger|null $logger
     */
    public function __construct(WhereParametersOperatorGeneratorInterface $generator, ?Logger $logger)
    {
        $this->generator = $generator;
        $this->logger = $logger;
    }

    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void {}

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, int $level, string $namePrefix): ?array
    {
        if (0 === count($object->getFields())) {
            return null;
        }

	    $name = "not";
        if (0 !== strlen($namePrefix)) {
            $name = sprintf("%s%s", $namePrefix, "_not");
        }

        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Generated NOT operator for object. Code %s", $name),
                [
                    "object" => $object,
			        "level" => $level,
			        "input-object-name" => $name,
                ]
            );
        }

        // Уровень сложности оператора - 0, поэтому нет смысла инкременировать текущий уровень
        $fields = $this->generator->generateOperations($object, $name, $level);
        if (null === $fields) {
            return null;
        }

	    return [
            "description" => "Оператор NOT для объединения условий",
		    "type" => new InputObjectType([
                "name" => $name,
                "fields" => $fields,
                "description" => "Объект листинга для оператора NOT для объединения условий",
            ]),
        ];
    }

    /**
     * Код операции, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operationCode(): string
    {
        return SBuilderConstants::NOT_SCHEMA_KEY;
    }
}