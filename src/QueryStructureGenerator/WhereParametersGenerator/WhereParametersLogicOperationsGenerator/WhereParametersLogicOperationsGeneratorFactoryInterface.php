<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use Monolog\Logger;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

/**
 * Фабрика сервиса
 */
interface WhereParametersLogicOperationsGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param WhereParametersOperatorGeneratorInterface $generator
     * @param Logger|null $logger
     * @return WhereParametersLogicOperationsGeneratorInterface
     */
    public static function make(
        WhereParametersOperatorGeneratorInterface $generator,
        ?Logger $logger
    ): WhereParametersLogicOperationsGeneratorInterface;
}