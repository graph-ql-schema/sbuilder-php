<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

/**
 * Генератор логических операций для Where
 */
class WhereParametersLogicOperationsGenerator implements WhereParametersLogicOperationsGeneratorInterface
{
    /** @var WhereParametersLogicOperationsGeneratorProcessorInterface[] */
    private $processors;

    /**
     * WhereParametersLogicOperationsGenerator constructor.
     * @param WhereParametersLogicOperationsGeneratorProcessorInterface[] $processors
     */
    public function __construct(WhereParametersLogicOperationsGeneratorProcessorInterface ...$processors)
    {
        $this->processors = $processors;
    }

    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void
    {
        foreach ($this->processors as $processor) {
            $processor->setWhereParametersOperatorGenerator($whereParametersGenerator);
        }
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generateParametersForObject(ObjectType $object, int $level, string $namePrefix): ?array
    {
        $result = [];
        foreach ($this->processors as $processor) {
            $generationResult = $processor->generate($object, $level, $namePrefix);
            if (null === $generationResult) {
                continue;
            }

            $result[$processor->operationCode()] = $generationResult;
        }

        if (0 === count($result)) {
            return null;
        }

        return $result;
    }
}