<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\Constants;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

/**
 * Фабрика сервиса
 */
class WhereParametersLogicOperationsGeneratorFactory implements WhereParametersLogicOperationsGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param WhereParametersOperatorGeneratorInterface $generator
     * @param Logger|null $logger
     * @return WhereParametersLogicOperationsGeneratorInterface
     */
    public static function make(
        WhereParametersOperatorGeneratorInterface $generator,
        ?Logger $logger
    ): WhereParametersLogicOperationsGeneratorInterface {
        return new WhereParametersLogicOperationsGenerator(
            new NotWhereParametersLogicOperationsGeneratorProcessor($generator, $logger),
            new WhereParametersLogicOperationsGeneratorProcessor(
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                "AND",
                SBuilderConstants::AND_SCHEMA_KEY,
                $logger
            ),
            new WhereParametersLogicOperationsGeneratorProcessor(
                Constants::MAX_DEPTH_LEVEL_FOR_RELATIONS,
                "OR",
                SBuilderConstants::OR_SCHEMA_KEY,
                $logger
            )
        );
    }
}