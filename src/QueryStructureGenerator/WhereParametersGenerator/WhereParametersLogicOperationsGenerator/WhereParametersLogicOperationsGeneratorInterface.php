<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

/**
 * Генератор логических операций для Where
 */
interface WhereParametersLogicOperationsGeneratorInterface
{
    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void;

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generateParametersForObject(ObjectType $object, int $level, string $namePrefix): ?array;
}