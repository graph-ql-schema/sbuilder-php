<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Monolog\Logger;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

/**
 * Процессор генератора оператора "and" и "or"
 */
class WhereParametersLogicOperationsGeneratorProcessor implements WhereParametersLogicOperationsGeneratorProcessorInterface
{
    /** @var WhereParametersGeneratorInterface */
    private $generator;

    /** @var Logger|null */
    private $logger;

    /** @var int */
    private $maxLevel;

    /** @var string */
    private $operationName;

    /** @var string */
    private $operationCode;

    /**
     * WhereParametersLogicOperationsGeneratorProcessor constructor.
     *
     * @param int $maxLevel
     * @param string $operationName
     * @param string $operationCode
     * @param Logger|null $logger
     */
    public function __construct(
        int $maxLevel,
        string $operationName,
        string $operationCode,
        ?Logger $logger
    )
    {
        $this->logger = $logger;
        $this->maxLevel = $maxLevel;
        $this->operationName = $operationName;
        $this->operationCode = $operationCode;
    }

    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void
    {
        $this->generator = $whereParametersGenerator;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, int $level, string $namePrefix): ?array
    {
        if (0 === count($object->getFields()) || $level > $this->maxLevel) {
            return null;
        }

	    $name = str_replace("_", "", $this->operationCode);
        if (0 !== strlen($namePrefix)) {
            $name = sprintf("%s%s", $namePrefix, $this->operationCode);
        }

        if (null !== $this->logger) {
            $this->logger->debug(
                Sprintf("Generated %s operator for object. Code: %s", $this->operationName, $name),
                [
                    "object" => $object,
                    "level" => $level,
                    "input-object-name" => $name,
                ]
            );
        }

        $fields = $this->generator->buildParameters($object, $name, $level + 1);
        if (null === $fields) {
            return null;
        }

        return [
            "description" => sprintf("Оператор %s для объединения условий", $this->operationName),
		    "type" => Type::listOf(Type::nonNull(
		        new InputObjectType([
                    "name" => $name,
                    "fields" => $fields,
                    "description" => sprintf("Объект листинга для оператора %s для объединения условий", $this->operationName),
                ])
            )),
        ];
    }

    /**
     * Код операции, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operationCode(): string
    {
        return $this->operationCode;
    }
}