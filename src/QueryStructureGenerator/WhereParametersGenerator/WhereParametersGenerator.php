<?php

namespace SBuilder\QueryStructureGenerator\WhereParametersGenerator;

use GraphQL\Type\Definition\ObjectType;
use Monolog\Logger;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

/**
 * Генератор параметров Where для запросов GraphQL
 */
class WhereParametersGenerator implements WhereParametersGeneratorInterface
{
    /** @var WhereParametersLogicOperationsGeneratorInterface */
    private $logicOperationsGenerator;

    /** @var WhereParametersOperatorGeneratorInterface */
    private $operatorGenerator;

    /** @var Logger|null */
    private $logger;

    /**
     * WhereParametersGenerator constructor.
     *
     * @param WhereParametersLogicOperationsGeneratorInterface $logicOperationsGenerator
     * @param WhereParametersOperatorGeneratorInterface $operatorGenerator
     * @param Logger|null $logger
     */
    public function __construct(
        WhereParametersLogicOperationsGeneratorInterface $logicOperationsGenerator,
        WhereParametersOperatorGeneratorInterface $operatorGenerator,
        ?Logger $logger
    ) {
        $this->logicOperationsGenerator = $logicOperationsGenerator;
        $this->operatorGenerator = $operatorGenerator;
        $this->logger = $logger;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array|null
     */
    public function buildParameters(ObjectType $object, string $namePrefix, int $level): ?array
    {
        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Started generation parameters for prefix '%s'", $namePrefix),
                [
                    "object" => $object,
                    "namePrefix" => $namePrefix,
                    "level" => $level,
                ]
            );
        }

        $fieldsConfig = $this->operatorGenerator->generateOperations($object, $namePrefix, $level);
        $operatorsConfig = $this->logicOperationsGenerator->generateParametersForObject($object, $level, $namePrefix);

        $result = array_merge([], $fieldsConfig ? $fieldsConfig : [], $operatorsConfig ? $operatorsConfig : []);

        return 0 === count($result) ? null : $result;
    }
}