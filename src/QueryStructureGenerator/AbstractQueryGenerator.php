<?php

namespace SBuilder\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;
use SBuilder\QueryStructureGenerator\GenerationProcessors\QueryGeneratorProcessorInterface;

/**
 * Абстрактный генератор запроса
 */
abstract class AbstractQueryGenerator implements QueryGeneratorInterface
{
    /** @var QueryGeneratorProcessorInterface[] */
    protected $processors;

    /** @var AliasGenerationServiceInterface */
    protected $aliasGenerator;

    /**
     * AbstractQueryGenerator constructor.
     *
     * @param QueryGeneratorProcessorInterface[] $processors
     * @param AliasGenerationServiceInterface $aliasGenerator
     */
    public function __construct(array $processors, AliasGenerationServiceInterface $aliasGenerator)
    {
        $this->processors = $processors;
        $this->aliasGenerator = $aliasGenerator;
    }

    /**
     * Генерация аргументов для запроса по процессорам запроса
     *
     * @param ObjectType $object
     * @return array|null
     */
    protected function generateArguments(ObjectType $object): ?array {
        $arguments = [];
        $name = sprintf('%s%s', $this->aliasGenerator->generate($object->name), $this->suffix());

        foreach ($this->processors as $processor) {
            $config = $processor->generate($object, $name);
            if (null === $config) {
                continue;
            }

            $arguments[$processor->getName()] = $config;
        }

        return 0 !== count($arguments) ? $arguments : null;
    }
}