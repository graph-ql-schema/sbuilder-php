<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator;

use GqlDatetime\Scalars\DateScalarType;
use GqlDatetime\Scalars\DateTimeScalarType;
use GqlDatetime\Scalars\TimeScalarType;
use GqlRootTypeGetter\GraphQlRootTypeGetter;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\ObjectType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ConfigurableAggregateFieldsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\CountAggregateFieldsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\VariantsAggregateFieldsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver\SubFieldsResolverFactory;

/**
 * Фабрика сервиса
 */
class AggregateFieldsGeneratorFactory implements AggregateFieldsGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return AggregateFieldsGeneratorInterface
     */
    public static function make(?Logger $logger): AggregateFieldsGeneratorInterface
    {
        $typeGetter = new GraphQlRootTypeGetter();
        $subFieldsFactory = new SubFieldsResolverFactory();
        return new AggregateFieldsGenerator(
            [
                new CountAggregateFieldsGeneratorProcessor($typeGetter, $logger),
                new VariantsAggregateFieldsGeneratorProcessor($typeGetter, $subFieldsFactory, $logger),
                new ConfigurableAggregateFieldsGeneratorProcessor(
                    $typeGetter,
                    $subFieldsFactory,
                    SBuilderConstants::SUM_OPERATION_SCHEMA_KEY,
                    [
                        IntType::class,
                        FloatType::class,
                        DateScalarType::class,
                        TimeScalarType::class,
                        DateTimeScalarType::class,
                    ],
                    function (ObjectType $objectType): string {
                        return sprintf("Получение суммы значений для полей сущности '%s'", $objectType->name);
                    },
                    function (string $code, ObjectType $objectType): string {
                        return sprintf("Получение суммы значений для поля %s", $code);
                    },
                    $logger
                ),
                new ConfigurableAggregateFieldsGeneratorProcessor(
                    $typeGetter,
                    $subFieldsFactory,
                    SBuilderConstants::AVG_OPERATION_SCHEMA_KEY,
                    [
                        IntType::class,
                        FloatType::class,
                        DateScalarType::class,
                        TimeScalarType::class,
                        DateTimeScalarType::class,
                    ],
                    function (ObjectType $objectType): string {
                        return sprintf("Получение средних значений для полей сущности '%s'", $objectType->name);
                    },
                    function (string $code, ObjectType $objectType): string {
                        return sprintf("Получение среднего значения для поля %s", $code);
                    },
                    $logger
                ),
                new ConfigurableAggregateFieldsGeneratorProcessor(
                    $typeGetter,
                    $subFieldsFactory,
                    SBuilderConstants::MAX_OPERATION_SCHEMA_KEY,
                    [
                        IntType::class,
                        FloatType::class,
                        DateScalarType::class,
                        TimeScalarType::class,
                        DateTimeScalarType::class,
                    ],
                    function (ObjectType $objectType): string {
                        return sprintf("Получение максимальных значений для полей сущности '%s'", $objectType->name);
                    },
                    function (string $code, ObjectType $objectType): string {
                        return sprintf("Получение максимального значения для поля %s", $code);
                    },
                    $logger
                ),
                new ConfigurableAggregateFieldsGeneratorProcessor(
                    $typeGetter,
                    $subFieldsFactory,
                    SBuilderConstants::MIN_OPERATION_SCHEMA_KEY,
                    [
                        IntType::class,
                        FloatType::class,
                        DateScalarType::class,
                        TimeScalarType::class,
                        DateTimeScalarType::class,
                    ],
                    function (ObjectType $objectType): string {
                        return sprintf("Получение минимальных значений для полей сущности '%s'", $objectType->name);
                    },
                    function (string $code, ObjectType $objectType): string {
                        return sprintf("Получение минимального значения для поля %s", $code);
                    },
                    $logger
                ),
            ],
            $logger
        );
    }
}