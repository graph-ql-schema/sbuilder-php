<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator;

use GraphQL\Type\Definition\ObjectType;
use Monolog\Logger;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\AggregateFieldsGeneratorProcessorInterface;

/**
 * Генератор аггрегирующей сущности для модели
 */
class AggregateFieldsGenerator implements AggregateFieldsGeneratorInterface
{
    /** @var AggregateFieldsGeneratorProcessorInterface[] */
    private $processors;

    /** @var Logger|null */
    private $logger;

    /**
     * AggregateFieldsGenerator constructor.
     *
     * @param AggregateFieldsGeneratorProcessorInterface[] $processors
     * @param Logger|null $logger
     */
    public function __construct(array $processors, ?Logger $logger)
    {
        $this->processors = $processors;
        $this->logger = $logger;
    }

    /**
     * Генерация сущности
     *
     * @param ObjectType $object
     * @return ObjectType
     */
    public function generate(ObjectType $object): ObjectType
    {
        if (null !== $this->logger) {
            $this->logger->debug(
                sprintf("Started generation of aggregation entity for object: %s", $object->name)
            );
        }

        $name = str_replace(" ", "_", strtolower($object->name));
	    $fields = [];

	    foreach ($this->processors as $processor) {
	        $generationResult = $processor->generateField($object);
	        if (null === $generationResult) {
	            continue;
            }

	        $fields[$generationResult->fieldCode] = $generationResult->field;
        }

	    return new ObjectType([
	        'name' => sprintf("%s_aggregation", $name),
            'fields' => $fields,
            'description' => sprintf("Сущность для запросов аггрегации для объекта: '%s'", $object->name),
        ]);
    }
}