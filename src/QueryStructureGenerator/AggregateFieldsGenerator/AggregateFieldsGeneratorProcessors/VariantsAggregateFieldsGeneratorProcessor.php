<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use GraphQlNullableField\Nullable;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver\SubFieldsResolverFactoryInterface;

/**
 * Генератор поля вариантов для аггрегирующей сущности
 */
class VariantsAggregateFieldsGeneratorProcessor implements AggregateFieldsGeneratorProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var Logger|null */
    private $logger;

    /** @var SubFieldsResolverFactoryInterface */
    private $fieldResolverFactory;

    /**
     * VariantsAggregateFieldsGeneratorProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param SubFieldsResolverFactoryInterface $fieldResolverFactory
     * @param Logger|null $logger
     */
    public function __construct(
        GraphQlRootTypeGetterInterface $typeGetter,
        SubFieldsResolverFactoryInterface $fieldResolverFactory,
        ?Logger $logger
    ) {
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
        $this->fieldResolverFactory = $fieldResolverFactory;
    }

    /**
     * Генерация поля для сущности
     *
     * @param ObjectType $object
     * @return ProcessResult
     */
    public function generateField(ObjectType $object): ?ProcessResult
    {
        $fields = $this->getAvailableFields($object);
        if (0 === count($fields)) {
            return null;
        }

        if (null != $this->logger) {
            $this->logger->debug(
                sprintf("Generation aggregation field '%s' for object: %s", SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY, $object->name)
            );
        }

        $namePrefix= sprintf("%s_aggregate_variants_field", str_replace(" ", "_", strtolower($object->name)));

        $fieldConfig = [];
        foreach ($fields as $code => $field) {
            $fType = $this->typeGetter->getRootType($field->getType());
            if ($this->typeGetter->isNullable($field->getType()) && $fType instanceof ScalarType) {
                $fType = Nullable::create($fType);
            }

            $fieldConfig[$code] = [
                'type' => Type::listOf($fType),
                'description' => sprintf("Получение списка уникальных значений для поля %s", $code),
                'resolve' => $this->fieldResolverFactory->make($code),
            ];
        }

        return new ProcessResult(
            [
                'type' => Type::nonNull(new ObjectType([
                    'name' => sprintf("%s_response_type", $namePrefix),
                    'fields' => $fieldConfig,
                    'description' => sprintf("Доступный для выбора список полей для получения уникальных значений по ним для сущности '%s'", $object->name),
                ])),
                'description' => sprintf("Получение списка уникальных значений для полей сущности '%s'", $object->name),
            ],
            SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY
        );
    }

    /**
     * Получение доступных для генерации полей
     *
     * @param ObjectType $objectType
     * @return FieldDefinition[]
     */
    private function getAvailableFields(ObjectType $objectType): array {
        $result = [];
        foreach ($objectType->getFields() as $code => $field) {
            $type = $this->typeGetter->getRootType($field->getType());
            if ($type instanceof ObjectType || $type instanceof UnionType) {
                continue;
            }

            $result[$code] = $field;
        }

        return $result;
    }
}
