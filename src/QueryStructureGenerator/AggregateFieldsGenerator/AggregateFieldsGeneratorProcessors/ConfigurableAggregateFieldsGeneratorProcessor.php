<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Monolog\Logger;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver\SubFieldsResolverFactoryInterface;

/**
 * Настраиваемый процессор для генерации полей аггрегирующей сущности
 */
class ConfigurableAggregateFieldsGeneratorProcessor implements AggregateFieldsGeneratorProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var Logger|null */
    private $logger;

    /** @var string[] */
    private $availableTypeClasses;

    /** @var string */
    private $operatorType;

    /** @var callable */
    private $valueDescriptionGenerator;

    /** @var SubFieldsResolverFactoryInterface */
    private $fieldResolverFactory;

    /** @var callable */
    private $operatorDescription;

    /**
     * ConfigurableAggregateFieldsGeneratorProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param SubFieldsResolverFactoryInterface $fieldResolverFactory
     * @param string $operatorType
     * @param array $availableTypeClasses
     * @param callable $operatorDescription
     * @param callable $valueDescriptionGenerator
     * @param Logger|null $logger
     */
    public function __construct(
        GraphQlRootTypeGetterInterface $typeGetter,
        SubFieldsResolverFactoryInterface $fieldResolverFactory,
        string $operatorType,
        array $availableTypeClasses,
        callable $operatorDescription,
        callable $valueDescriptionGenerator,
        ?Logger $logger
    ){
        $this->typeGetter = $typeGetter;
        $this->fieldResolverFactory = $fieldResolverFactory;
        $this->operatorType = $operatorType;
        $this->availableTypeClasses = $availableTypeClasses;
        $this->operatorDescription = $operatorDescription;
        $this->valueDescriptionGenerator = $valueDescriptionGenerator;
        $this->logger = $logger;
    }

    /**
     * Генерация поля для сущности
     *
     * @param ObjectType $object
     * @return ProcessResult
     */
    public function generateField(ObjectType $object): ?ProcessResult
    {
        $fields = $this->getAvailableFields($object);
        if (0 === count($fields)) {
            return null;
        }

        if (null !== $this->logger) {
            $this->logger->debug(sprintf("Generation aggregation field '%s' for object: %s", $this->operatorType, $object->name));
        }

        $namePrefix = sprintf(
            "%s_aggregate_%s_field",
            str_replace(" ", "_", strtolower($object->name)),
            strtolower($this->operatorType)
        );

        $fieldsConf = [];
        $valueDescriptionGenerator = $this->valueDescriptionGenerator;
        foreach ($fields as $code => $field) {
            $fType = $this->typeGetter->getRootType($field->getType());
            if ($fType instanceof IntType) {
                $fType = Type::float();
            }

            $fieldsConf[$code] = [
                'type' => $fType,
                'description' => $valueDescriptionGenerator($code, $object),
                'resolve' => $this->fieldResolverFactory->make($code)
            ];
        }

        $operatorDescription = $this->operatorDescription;
        return new ProcessResult(
            [
                "type" => Type::nonNull(new ObjectType([
                    "name" => sprintf("%s_response_type", $namePrefix),
                    "fields" => $fieldsConf,
                    "description" => $operatorDescription($object),
                ])),
                "description" => $operatorDescription($object),
            ],
            $this->operatorType
        );
    }

    /**
     * Получение доступных для генерации полей
     *
     * @param ObjectType $object
     * @return FieldDefinition[]
     */
    private function getAvailableFields(ObjectType $object): array {
	    $result = [];
	    foreach ($object->getFields() as $code => $field) {
	        $type = $this->typeGetter->getRootType($field->getType());
	        $typeClass = get_class($type);

	        if (!in_array($typeClass, $this->availableTypeClasses)) {
	            continue;
            }

	        $result[$code] = $field;
        }

	    return $result;
    }
}