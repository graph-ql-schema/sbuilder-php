<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

/**
 * Результат генерации процессора
 */
class ProcessResult {
    /** @var array */
    public $field;

    /** @var string */
    public $fieldCode;

    /**
     * ProcessResult constructor.
     *
     * @param array $field
     * @param string $fieldCode
     */
    public function __construct(array $field, string $fieldCode)
    {
        $this->field = $field;
        $this->fieldCode = $fieldCode;
    }
}