<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;

/**
 * Процессор для генерации аггрегирующей сущности. Генерирует поле 'count'
 */
class CountAggregateFieldsGeneratorProcessor implements AggregateFieldsGeneratorProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var Logger|null */
    private $logger;

    /**
     * CountAggregateFieldsGeneratorProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param Logger|null $logger
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter, ?Logger $logger)
    {
        $this->typeGetter = $typeGetter;
        $this->logger = $logger;
    }

    /**
     * Генерация поля для сущности
     *
     * @param ObjectType $object
     * @return ProcessResult
     */
    public function generateField(ObjectType $object): ?ProcessResult
    {
        if (false === $this->isSimpleFieldAvailable($object)) {
            return null;
        }

        if (null !== $this->logger) {
            $this->logger->debug(sprintf("Generation aggregation field '%s' for object: %s", SBuilderConstants::COUNT_OPERATION_SCHEMA_KEY, $object->name));
        }

        return new ProcessResult(
            [
                'type' => Type::int(),
                'description' => Sprintf(
                    "Получение количества элементов сущности во всей таблице или в каждой группе отдельно, при наличии GroupBy для сущности: '%s'",
                    $object->name
                ),
            ],
            SBuilderConstants::COUNT_OPERATION_SCHEMA_KEY
        );
    }

    /**
     * Проверяет доступность простого поля в объекте. По сути метод получения количества должен быть доступен
     * только для объектов с простыми полями, количество значений которых возможно вычислить.
     *
     * @param ObjectType $objectType
     * @return bool
     */
    private function isSimpleFieldAvailable(ObjectType $objectType): bool
    {
        foreach ($objectType->getFields() as $field) {
            $fType = $this->typeGetter->getRootType($field->getType());
            if ($fType instanceof ObjectType || $fType instanceof UnionType) {
                continue;
            }

            return true;
        }

        return false;
    }
}
