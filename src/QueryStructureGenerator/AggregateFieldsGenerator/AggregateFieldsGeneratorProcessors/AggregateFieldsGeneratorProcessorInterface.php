<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор для генерации аггрегирующей сущности
 */
interface AggregateFieldsGeneratorProcessorInterface
{
    /**
     * Генерация поля для сущности
     *
     * @param ObjectType $object
     * @return ProcessResult
     */
    public function generateField(ObjectType $object): ?ProcessResult;
}