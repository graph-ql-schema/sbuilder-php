<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver;

/**
 * Резолвер поля для аггрегирующей сущности
 */
interface SubFieldsResolverInterface
{
    /**
     * Резолвинг полей для аггрегирующей сущности
     *
     * @param mixed $parentData
     * @return mixed
     */
    public function resolve($parentData);
}