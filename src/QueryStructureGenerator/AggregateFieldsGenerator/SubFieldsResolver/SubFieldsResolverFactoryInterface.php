<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver;

/**
 * Интерфейс резолвера поля для аггрегирующей сущности
 */
interface SubFieldsResolverFactoryInterface
{
    /**
     * Фабричный метод генерации callback функции для резолвинга поля для аггрегирующей сущности
     *
     * @param string $fieldCode
     * @return callable
     */
    public static function make(string $fieldCode): callable;
}