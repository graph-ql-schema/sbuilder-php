<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver;

/**
 * Интерфейс резолвера поля для аггрегирующей сущности
 */
class SubFieldsResolverFactory implements SubFieldsResolverFactoryInterface
{
    /**
     * Фабричный метод генерации callback функции для резолвинга поля для аггрегирующей сущности
     *
     * @param string $fieldCode
     * @return callable
     */
    public static function make(string $fieldCode): callable
    {
        $instance = new SubFieldsResolver($fieldCode);
        return [$instance, "resolve"];
    }
}