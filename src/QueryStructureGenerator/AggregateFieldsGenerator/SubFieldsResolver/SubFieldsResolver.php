<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver;

use LogicException;
use Throwable;

/**
 * Резолвер поля для аггрегирующей сущности
 */
class SubFieldsResolver implements SubFieldsResolverInterface
{
    /**
     * @var string
     */
    private $fieldCode;

    /**
     * SubFieldsResolver constructor.
     *
     * @param string $fieldCode
     */
    public function __construct(string $fieldCode)
    {
        $this->fieldCode = $fieldCode;
    }

    /**
     * Резолвинг полей для аггрегирующей сущности
     *
     * @param mixed $parentData
     * @return mixed
     */
    public function resolve($parentData)
    {
        if (!$parentData) {
            return null;
	    }

        if (!is_array($parentData)) {
            try {
                $parentData = json_decode(json_encode($parentData), true);
            } catch (Throwable $exception) {
                throw new LogicException("Can't convert data to simple collection.", 500, $exception);
            }
        }

        if (!is_array($parentData)) {
            throw new LogicException("You should pass correct parent data. Parent data is not an array.", 500);
        }

        if (!array_key_exists($this->fieldCode, $parentData)) {
            return null;
        }

		$data = $parentData[$this->fieldCode];
        if (!is_array($data)) {
            return $data;
        }

        return $this->uniqueData($data);
	}

    /**
     * Уникализация переданных данных
     *
     * @param array $data
     * @return array
     */
	private function uniqueData(array $data): array {
	    $result = [];
	    foreach ($data as $item) {
            $itemKey = json_encode($item);
            if (!array_key_exists($itemKey, $result)) {
                $result[$itemKey] = $item;
            }
        }

        return array_values($result);
    }
}