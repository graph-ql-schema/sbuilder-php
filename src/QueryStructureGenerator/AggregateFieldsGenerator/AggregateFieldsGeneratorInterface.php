<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator;

use GraphQL\Type\Definition\ObjectType;

/**
 * Интерфейс генератора аггрегирующей сущности для модели
 */
interface AggregateFieldsGeneratorInterface
{
    /**
     * Генерация сущности
     *
     * @param ObjectType $object
     * @return ObjectType
     */
    public function generate(ObjectType $object): ObjectType;
}