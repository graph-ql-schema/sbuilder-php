<?php

namespace SBuilder\QueryStructureGenerator\AggregateFieldsGenerator;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface AggregateFieldsGeneratorFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return AggregateFieldsGeneratorInterface
     */
    public static function make(?Logger $logger): AggregateFieldsGeneratorInterface;
}