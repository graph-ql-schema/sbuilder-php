<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\ObjectType;

/**
 * Процессор генерации запроса
 */
interface QueryGeneratorProcessorInterface
{
    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array;
}