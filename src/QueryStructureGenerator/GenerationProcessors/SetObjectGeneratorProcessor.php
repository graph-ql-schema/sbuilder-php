<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use GraphQlNullableField\Nullable;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;

/**
 * Процессор генерации объекта SET для обновления сущности.
 */
class SetObjectGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var AliasGenerationServiceInterface */
    private $aliasGenerator;

    /**
     * SetObjectGeneratorProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param AliasGenerationServiceInterface $aliasGenerator
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter, AliasGenerationServiceInterface $aliasGenerator)
    {
        $this->typeGetter = $typeGetter;
        $this->aliasGenerator = $aliasGenerator;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return SBuilderConstants::SET_SCHEMA_KEY;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        $name = sprintf("%s_set", $namePrefix);
        $updateObject = $this->createUpdateObject($object, $name);
        if (null === $updateObject) {
            return null;
        }

        return [
            'type' => new InputObjectType($updateObject),
		    'description' => "Сущность для обновления",
        ];
    }

    /**
     * Генерация объекта для обновления
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    private function createUpdateObject(ObjectType $object, string $namePrefix): ?array {
        $fieldsConfig = $this->getFieldsConfigMapByObject($object);
        if (null === $fieldsConfig) {
            return null;
        }

        return [
            'name' => sprintf("%s_type", $namePrefix),
            'fields' => $fieldsConfig,
            'description' => sprintf("Тип объекта для обновления значения для сущности: '%s'", $object->name),
        ];
    }

    /**
     * Генерация коллекции значений для вставки
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    private function getFieldsConfigMapByObject(ObjectType $object): ?array {
        $configMap = [];
        foreach ($object->getFields() as $code => $field) {
            $fieldType = $this->typeGetter->getRootType($field->getType());
            if ($fieldType instanceof ObjectType || $fieldType instanceof UnionType) {
                continue;
            }

            if ($fieldType instanceof ScalarType && $this->typeGetter->isNullable($fieldType)) {
                $fieldType = Nullable::create($fieldType);
            }
    
            if ($this->typeGetter->isList($field->getType())) {
                $fieldType = Type::listOf($fieldType);
            }

            $configMap[$code] = [
                'type' => $fieldType,
                'description' => sprintf(
                    "Значение для обновления для сущности '%s' поля '%s'",
                    $object->name,
                    $this->aliasGenerator->generate($field->name)
                ),
            ];
        }

        return 0 !== count($configMap) ? $configMap : null;
    }
}
