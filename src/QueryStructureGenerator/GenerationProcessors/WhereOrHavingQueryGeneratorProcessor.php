<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

/**
 * Процессор генерации параметров Where
 */
class WhereOrHavingQueryGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var WhereParametersGeneratorInterface */
    private $whereGenerator;

    /** @var string */
    private $name;

    /**
     * WhereOrHavingQueryGeneratorProcessor constructor.
     *
     * @param WhereParametersGeneratorInterface $whereGenerator
     * @param string $name
     */
    public function __construct(WhereParametersGeneratorInterface $whereGenerator, string $name)
    {
        $this->whereGenerator = $whereGenerator;
        $this->name = $name;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        $namePrefixImproved = sprintf(
            "%s_query_%s",
            $namePrefix,
            $this->name
        );

        $fieldsConfig = $this->whereGenerator->buildParameters($object, $namePrefixImproved, 1);
        if (null === $fieldsConfig) {
            return null;
        }

        return [
            'type' => new InputObjectType([
                'name' => sprintf("%s_object", $namePrefixImproved),
                'fields' => $fieldsConfig,
                'description' => sprintf("%s: Фильтр '%s' для листинга сущностей.", $object->name, $this->name),
            ]),
            'description' => sprintf("%s: Фильтр '%s' для листинга сущностей.", $object->name, $this->name),
        ];
    }
}