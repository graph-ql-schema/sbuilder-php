<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;

/**
 * Генерация параметра для вставки значения
 */
class ObjectInsertGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var GraphQlRootTypeGetterInterface */
    private $typeGetter;

    /** @var AliasGenerationServiceInterface */
    private $aliasGenerator;

    /**
     * ObjectInsertGeneratorProcessor constructor.
     *
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param AliasGenerationServiceInterface $aliasGenerator
     */
    public function __construct(GraphQlRootTypeGetterInterface $typeGetter, AliasGenerationServiceInterface $aliasGenerator)
    {
        $this->typeGetter = $typeGetter;
        $this->aliasGenerator = $aliasGenerator;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return SBuilderConstants::OBJECTS_SCHEMA_KEY;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        $name = sprintf("%s_object", $namePrefix);
        $insertObject = $this->createInsertObject($object, $name);
        if (null === $insertObject) {
            return null;
        }

        return [
            'type' => Type::listOf(new InputObjectType($insertObject)),
		    'description' => "Массив сущностей для вставки",
        ];
    }

    /**
     * Генерация объекта для вставки
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    private function createInsertObject(ObjectType $object, string $namePrefix): ?array {
        $fieldsConfig = $this->getFieldsConfigMapByObject($object);
        if (null === $fieldsConfig) {
            return null;
        }

        return [
            'name' => sprintf("%s_type", $namePrefix),
            'fields' => $fieldsConfig,
            'description' => sprintf("Тип объекта для вставки значения для сущности: '%s'", $object->name),
        ];
    }

    /**
     * Генерация конфигурации полей для объекта вставки
     *
     * @param ObjectType $object
     * @return array|null
     */
    private function getFieldsConfigMapByObject(ObjectType $object): ?array {
	    $configMap = [];
        foreach($object->getFields() as $fieldName => $field) {
            $fieldRootType = $this->typeGetter->getRootType($field->getType());
            if ($fieldRootType instanceof ObjectType || $fieldRootType instanceof UnionType) {
                continue;
            }

            $configMap[$fieldName] = [
                'type' => $field->getType(),
                'description' => sprintf(
                    "Значение для вставки для сущности '%s' поля '%s'",
                    $object->name,
                    $this->aliasGenerator->generate($field->name)
                ),
            ];
        }

        if (0 === count($configMap)) {
            return null;
        }

        return $configMap;
    }
}
