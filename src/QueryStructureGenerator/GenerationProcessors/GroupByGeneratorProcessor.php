<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\GroupByParametersGenerator\GroupByParametersGeneratorInterface;

/**
 * Процессор для генерации параметров группировки
 */
class GroupByGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var GroupByParametersGeneratorInterface */
    private $groupByGenerator;

    /**
     * GroupByGeneratorProcessor constructor.
     *
     * @param GroupByParametersGeneratorInterface $groupByGenerator
     */
    public function __construct(GroupByParametersGeneratorInterface $groupByGenerator)
    {
        $this->groupByGenerator = $groupByGenerator;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return SBuilderConstants::GROUP_BY_SCHEMA_KEY;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        return $this->groupByGenerator->generate($object, $namePrefix);
    }
}