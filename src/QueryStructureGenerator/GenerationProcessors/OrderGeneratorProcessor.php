<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\OrderParametersGenerator\OrderParametersGeneratorInterface;

/**
 * Процессор генерации параметров сортировки
 */
class OrderGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var OrderParametersGeneratorInterface */
    private $orderParametersGenerator;

    /**
     * OrderGeneratorProcessor constructor.
     *
     * @param OrderParametersGeneratorInterface $orderParametersGenerator
     */
    public function __construct(OrderParametersGeneratorInterface $orderParametersGenerator)
    {
        $this->orderParametersGenerator = $orderParametersGenerator;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return SBuilderConstants::ORDER_BY_SCHEMA_KEY;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        return $this->orderParametersGenerator->generate($object, $namePrefix);
    }
}