<?php

namespace SBuilder\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\PaginationParametersGenerator\PaginationParametersGeneratorInterface;

/**
 * Процессор генерации параметров Offset
 */
class OffsetQueryGeneratorProcessor implements QueryGeneratorProcessorInterface
{
    /** @var PaginationParametersGeneratorInterface */
    private $paginationGenerator;

    /**
     * OffsetQueryGeneratorProcessor constructor.
     *
     * @param PaginationParametersGeneratorInterface $paginationGenerator
     */
    public function __construct(PaginationParametersGeneratorInterface $paginationGenerator)
    {
        $this->paginationGenerator = $paginationGenerator;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return SBuilderConstants::OFFSET_SCHEMA_KEY;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        return $this->paginationGenerator->generateOffset();
    }
}