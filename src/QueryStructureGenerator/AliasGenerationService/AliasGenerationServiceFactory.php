<?php


namespace SBuilder\QueryStructureGenerator\AliasGenerationService;


/**
 * @inheritDoc
 */
class AliasGenerationServiceFactory implements AliasGenerationFactoryInterface
{
    /**
     * @inheritDoc
     */
    public static function create(): AliasGenerationServiceInterface
    {
        return new AliasGenerationService([
            "a" => "a", "b" => "b", "c" => "c", "d" => "d", "e" => "e", "f" => "f", "g" => "g", "h" => "h", "i" => "i",
            "j" => "j", "k" => "k", "l" => "l", "m" => "m", "n" => "n", "o" => "o", "p" => "p", "q" => "q", "r" => "r",
            "s" => "s", "t" => "t", "u" => "u", "v" => "v", "w" => "w", "x" => "x", "y" => "y", "z" => "z", "-" => "_",
            " " => "_", "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e", "ж" => "j",
            "з" => "z", "и" => "i", "й" => "i", "к" => "k", "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h", "ц" => "c", "ч" => "ch", "ш" => "sh",
            "щ" => "sh", "ъ" => "", "ы" => "i", "ь" => "", "э" => "a", "ю" => "ju", "я" => "ja", "_" => "_",
        ]);
    }
}
