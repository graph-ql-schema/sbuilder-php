<?php

namespace SBuilder\QueryStructureGenerator\AliasGenerationService;

/**
 * Generate alias for current string.
 * Replace all forbidden symbols in a string with alternative ones and return result.
 *
 * @package SBuilder\QueryStructureGenerator\AliasGenerationService
 */
interface AliasGenerationServiceInterface
{
    /**
     * Alias generation.
     *
     * @param string $source
     * @return string
     */
    public function generate(string $source): string;
}
