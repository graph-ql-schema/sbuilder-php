<?php

namespace SBuilder\QueryStructureGenerator\AliasGenerationService;

use Exception;
use LogicException;

/**
 * @inheritDoc
 */
class AliasGenerationService implements AliasGenerationServiceInterface
{

    /**
     * @var array [string => string]
     */
    private $replacementMap;

    /**
     * AliasGenerationService constructor.
     * @param array $replacementMap
     */
    public function __construct(array $replacementMap)
    {
        $this->replacementMap = $replacementMap;
    }

    /**
     * @param string $source
     * @return string
     * @throws LogicException
     */
    public function generate(string $source): string
    {
        $literalsToReplace = [" " => "\s", "-" => "\-"];

        $result = strtolower($source);

        $includedSymbols = [];

        foreach ($this->replacementMap as $sourceSymbol => $_) {
            if (array_key_exists($sourceSymbol, $literalsToReplace)) {
                $sourceSymbol = $literalsToReplace[$sourceSymbol];
            }

            $includedSymbols[] = $sourceSymbol;
        }

        $pattern = sprintf("[^%s]", implode('', $includedSymbols));
        try {
            $result = mb_ereg_replace($pattern, '', $result);
        } catch (Exception $e) {
            throw new LogicException('Bad regex pattern.');
        }

        $result = str_replace(array_keys($this->replacementMap), array_values($this->replacementMap), $result);

        return $result;
    }
}
