<?php

namespace SBuilder\QueryStructureGenerator\AliasGenerationService;

/**
 * Factory for creating AliasGenerationService.
 *
 * @package SBuilder\QueryStructureGenerator\AliasGenerationService
 */
interface AliasGenerationFactoryInterface
{
    /**
     * @return AliasGenerationServiceInterface
     */
    public static function create(): AliasGenerationServiceInterface;
}
