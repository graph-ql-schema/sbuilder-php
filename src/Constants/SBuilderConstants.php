<?php

namespace SBuilder\Constants;

/**
 * Констранты библиотеки
 */
class SBuilderConstants
{
    const LIST_QUERY = "_list";
    const AGGREGATE_QUERY = "_aggregate";
    const INSERT_MUTATION = "_insert";
    const UPDATE_MUTATION = "_update";
    const DELETE_MUTATION = "_delete";

    const WHERE_SCHEMA_KEY = "where";
    const HAVING_SCHEMA_KEY = "having";
    const ORDER_BY_SCHEMA_KEY = "order";
    const GROUP_BY_SCHEMA_KEY = "groupBy";
    const OBJECTS_SCHEMA_KEY = "objects";
    const SET_SCHEMA_KEY = "set";
    const LIMIT_SCHEMA_KEY = "limit";
    const OFFSET_SCHEMA_KEY = "offset";

    const SUM_OPERATION_SCHEMA_KEY = "sum";
    const AVG_OPERATION_SCHEMA_KEY = "avg";
    const MAX_OPERATION_SCHEMA_KEY = "max";
    const MIN_OPERATION_SCHEMA_KEY = "min";
    const COUNT_OPERATION_SCHEMA_KEY = "count";
    const VARIANTS_OPERATION_SCHEMA_KEY = "variants";

    const AND_SCHEMA_KEY = "_and";
    const OR_SCHEMA_KEY = "_or";
    const NOT_SCHEMA_KEY = "_not";

    const BETWEEN_SCHEMA_KEY = "_between";
    const EQUALS_SCHEMA_KEY = "_equals";
    const IN_SCHEMA_KEY = "_in";
    const MORE_SCHEMA_KEY = "_more";
    const LESS_SCHEMA_KEY = "_less";
    const MORE_OR_EQUALS_SCHEMA_KEY = "_more_or_equals";
    const LESS_OR_EQUALS_SCHEMA_KEY = "_less_or_equals";
    const LIKE_SCHEMA_KEY = "_like";
}