<?php

namespace SBuilder;

use Monolog\Logger;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceFactory;
use SBuilder\QueryStructureGenerator\QueryFactory;
use SBuilder\RequestResolver\RequestResolverFactory;
use SBuilder\SchemaBuilder\SchemaBuilder;
use SBuilder\SchemaBuilder\SchemaBuilderInterface;

/**
 * Фасад библиотеки. Требуется для упрощения работы с ней.
 */
class GraphQlSchemaBuilder
{
    /**
     * Создает экземпляр генератора схемы
     *
     * @param Logger|null $logger
     * @return SchemaBuilderInterface
     */
    public static function make(?Logger $logger = null): SchemaBuilderInterface {
        return new SchemaBuilder(
            [
                SBuilderConstants::LIST_QUERY => QueryFactory::makeListGenerator($logger),
                SBuilderConstants::AGGREGATE_QUERY => QueryFactory::makeAggregateGenerator($logger),
                SBuilderConstants::INSERT_MUTATION => QueryFactory::makeInsertGenerator($logger),
                SBuilderConstants::UPDATE_MUTATION => QueryFactory::makeUpdateGenerator($logger),
                SBuilderConstants::DELETE_MUTATION => QueryFactory::makeDeleteGenerator($logger),
            ],
            RequestResolverFactory::make($logger),
            AliasGenerationServiceFactory::create()
        );
    }
}