<?php

namespace SBuilder\RequestParser\Types;

use GraphQL\Type\Definition\ResolveInfo;

/**
 * Params for fieldResolver function.
 */
class ResolveParams
{
    /** @var mixed */
    public $source;

    /** @var array */
    public $args;

    /** @var mixed */
    public $context;

    /** @var ResolveInfo */
    public $info;

    /**
     * @param mixed $source
     * @param array $args
     * @param mixed $context
     * @param ResolveInfo $info
     */
    public function __construct($source, array $args, $context, ResolveInfo $info)
    {
        $this->source = $source;
        $this->args = $args;
        $this->context = $context;
        $this->info = $info;
    }
}
