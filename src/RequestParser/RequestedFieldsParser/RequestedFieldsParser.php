<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

use GraphQL\Language\AST\FieldNode;
use GraphQL\Language\AST\FragmentDefinitionNode;
use GraphQL\Language\AST\FragmentSpreadNode;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\NodeList;
use GraphQL\Language\AST\SelectionNode;
use LogicException;
use Monolog\Logger;
use SBuilder\RequestParser\Types\ResolveParams;

/**
 * @inheritDoc
 */
class RequestedFieldsParser implements RequestedFieldsParserInterface
{
    /** @var Logger|null */
    private $logger;

    /**
     * RequestedFieldsParser constructor.
     *
     * @param Logger|null $logger
     */
    public function __construct(Logger $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function parseRequest(ResolveParams $params): array
    {
        if ($this->logger) {
            $this->logger->debug('Started request field parsing.', ['params' => $params]);
        }

        $fieldASTs = $params->info->fieldNodes;

        if (empty($fieldASTs)) {
            if ($this->logger) {
                $this->logger->debug('Fields to parse is not found in request.');
            }

            return [];
        }

        $result = $this->getFieldsFromSelection($params, $fieldASTs[0]->selectionSet->selections);
        if ($this->logger) {
            $this->logger->debug('Parsed requested fields.', ['result' => $result]);
        }

        return $result;
    }

    /**
     * Получение полей из переданного запроса
     *
     * @param ResolveParams $params
     * @param NodeList<SelectionNode&Node> $selections
     * @return array
     * @throws LogicException
     */
    private function getFieldsFromSelection(ResolveParams $params, NodeList $selections): array
    {
        $result = [];
        foreach ($selections as $selection) {
            $fieldName = $selection->name->value;
            switch (true) {
                case $selection instanceof FieldNode || $selection instanceof FragmentDefinitionNode:
                    $subFields = [];
                    if (!is_null($selection->selectionSet)) {
                        $subFields = $this->getFieldsFromSelection($params, $selection->selectionSet->selections);
                    }

                    if ($this->logger) {
                        $this->logger->debug("Parsed params for '${$fieldName}' field.");
                    }

                    $result[] = new RequestedField($fieldName, $subFields);
                    break;
                case $selection instanceof FragmentSpreadNode:
                    $frag = $params->info->fragments[$fieldName];
                    if (is_null($frag)) {
                        $errorMessage = "No fragment found for '${$fieldName}' field.";
                        if ($this->logger) {
                            $this->logger->debug($errorMessage);
                        }

                        throw new LogicException($errorMessage);
                    }

                    $subFields = [];
                    if (!is_null($frag->selectionSet)) {
                        $subFields = $this->getFieldsFromSelection($params, $frag->selectionSet->selections);
                    }

                    if ($this->logger) {
                        $this->logger->debug("Parsed params for '${$fieldName}' field.");
                    }

                    $result[] = new RequestedField($fieldName, $subFields);
                    break;
                default:
                    $errorMessage = 'Found unexpected selection field type.';
                    if ($this->logger) {
                        $this->logger->debug($errorMessage, ['selection' => $selection]);
                    }

                    throw new LogicException($errorMessage);
            }
        }

        return $result;
    }
}
