<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

/**
 * @inheritDoc
 */
class RequestedField implements RequestedFieldInterface
{
    /** @var string */
    private $fieldName;

    /** @var RequestedFieldInterface[] */
    private $subFields;

    /**
     * @param string $fieldName
     * @param array|null $subFields
     */
    public function __construct(string $fieldName, ?array $subFields = [])
    {
        $this->fieldName = $fieldName;
        $this->subFields = is_null($subFields) ? [] : $subFields;
    }

    /**
     * @inheritDoc
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @inheritDoc
     */
    public function hasSubFields(): bool
    {
        return !empty($this->subFields);
    }

    /**
     * @inheritDoc
     */
    public function getSubFields(): array
    {
        return $this->subFields;
    }
}
