<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface RequestedFieldsParserFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return RequestedFieldsParserInterface
     */
    public static function make(Logger $logger = null): RequestedFieldsParserInterface;
}