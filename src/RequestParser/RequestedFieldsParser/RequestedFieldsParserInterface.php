<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

use LogicException;
use SBuilder\RequestParser\Types\ResolveParams;

/**
 * Parser for request fields.
 */
interface RequestedFieldsParserInterface
{
    /**
     * Парсинг запроса
     *
     * @param ResolveParams $params
     * @return RequestedFieldInterface[]
     * @throws LogicException
     */
    public function parseRequest(ResolveParams $params): array;
}
