<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
class RequestedFieldsParserFactory implements RequestedFieldsParserFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return RequestedFieldsParserInterface
     */
    public static function make(Logger $logger = null): RequestedFieldsParserInterface
    {
        return new RequestedFieldsParser($logger);
    }
}