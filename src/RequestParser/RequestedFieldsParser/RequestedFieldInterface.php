<?php

namespace SBuilder\RequestParser\RequestedFieldsParser;

/**
 * Requested field entity.
 */
interface RequestedFieldInterface
{
    /**
     * Получение названия поля
     *
     * @return string
     */
    public function getFieldName(): string;

    /**
     * Есть ли дочерние поля
     *
     * @return bool
     */
    public function hasSubFields(): bool;

    /**
     * Получение дочерних полей
     *
     * @return RequestedFieldInterface[]
     */
    public function getSubFields(): array;
}
