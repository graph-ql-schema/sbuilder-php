<?php

namespace SBuilder\SchemaBuilder;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Schema;
use InvalidArgumentException;
use LogicException;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;
use SBuilder\QueryStructureGenerator\QueryGeneratorInterface;
use SBuilder\RequestResolver\RequestResolverInterface;
use SBuilder\RequestResolver\ResolverInterface;

/**
 * Интерфейс основного строителя схемы GraphQL
 */
class SchemaBuilder implements SchemaBuilderInterface
{
    /** @var QueryGeneratorInterface[] */
    private $generators;

    /** @var RequestResolverInterface */
    private $resolverGenerator;

    /** @var AliasGenerationServiceInterface */
    private $aliasGenerator;

    /** @var ObjectType[] */
    private $queries = [];

    /** @var ObjectType[] */
    private $mutations = [];

    private const AVAILABLE_QUERIES = [
        SBuilderConstants::LIST_QUERY,
        SBuilderConstants::AGGREGATE_QUERY,
        SBuilderConstants::INSERT_MUTATION,
        SBuilderConstants::UPDATE_MUTATION,
        SBuilderConstants::DELETE_MUTATION,
    ];

    /**
     * SchemaBuilder constructor.
     * @param QueryGeneratorInterface[] $generators
     * @param RequestResolverInterface $resolverGenerator
     * @param AliasGenerationServiceInterface $aliasGenerator
     */
    public function __construct(
        array $generators,
        RequestResolverInterface $resolverGenerator,
        AliasGenerationServiceInterface $aliasGenerator
    ) {
        $this->generators = $generators;
        $this->resolverGenerator = $resolverGenerator;
        $this->aliasGenerator = $aliasGenerator;
    }

    /**
     * Регистратор запросов для сущностей.
     *
     * Для регистрации первым параметром передается объект GraphQL, описывающий тип сущности.
     * Вторым параметром передается массив запросов, которые необходимо создать для этой сущности.
     * Например:
     * [
     *    SBuilderConstants::LIST_QUERY => new class implements ResolverInterface {...}
     * ]
     *
     * Доступные типы запросов:
     * SBuilderConstants::LIST_QUERY - Листинг сущностей
     * SBuilderConstants::AGGREGATE_QUERY - Аггрегация данных по сущностям
     * SBuilderConstants::INSERT_MUTATION - Мутация вставки сущностей
     * SBuilderConstants::UPDATE_MUTATION - Мутация обновления сущностей
     * SBuilderConstants::DELETE_MUTATION - Мутация удаления сущностей
     *
     * @param ObjectType $object
     * @param ResolverInterface[] $queries
     */
    public function registerEntity(ObjectType $object, array $queries): void
    {
        foreach ($queries as $type => $resolver) {
            if (!in_array($type, self::AVAILABLE_QUERIES)) {
                throw new InvalidArgumentException(sprintf("Passed incorrect query type: %s", $type), 500);
            }

            if (!$resolver instanceof ResolverInterface) {
                throw new InvalidArgumentException(sprintf("Resolver of type %s is not instance of %s", $type, ResolverInterface::class));
            }

            if (!array_key_exists($type, $this->generators)) {
                throw new LogicException(sprintf("Not found generator for query type: %s", $type));
            }

            $resolver = $this->resolverGenerator->generate($object, $resolver);
            $generator = $this->generators[$type];
            $key = sprintf("%s%s", $this->aliasGenerator->generate($object->name), $generator->suffix());

            $query = $generator->generate($object, $resolver);
            if (null === $query) {
                continue;
            }

            if (in_array($type, [SBuilderConstants::LIST_QUERY, SBuilderConstants::AGGREGATE_QUERY])) {
                $this->queries[$key] = $query;
                continue;
            }

            $this->mutations[$key] = $query;
        }
    }

    /**
     * Регистрация кастомного запроса
     *
     * @param string $queryName
     * @param array $query
     */
    public function registerQuery(string $queryName, array $query): void
    {
        $this->queries[$queryName] = $query;
    }

    /**
     * Регистрация кастомной мутации
     *
     * @param string $mutationName
     * @param array $mutation
     */
    public function registerMutation(string $mutationName, array $mutation): void
    {
        $this->mutations[$mutationName] = $mutation;
    }

    /**
     * Генерация схемы, пригодной для использования в GraphQL эндпоинте
     *
     * @return Schema
     */
    public function buildSchema(): Schema
    {
        return new Schema([
            'query' => 0 !== count($this->queries)
                ? new ObjectType([
                    'name' => 'Query',
                    'fields' => $this->queries,
                ])
                : null,
            'mutation' => 0 !== count($this->mutations)
                ? new ObjectType([
                    'name' => 'Mutation',
                    'fields' => $this->mutations,
                ])
                : null,
        ]);
    }
}