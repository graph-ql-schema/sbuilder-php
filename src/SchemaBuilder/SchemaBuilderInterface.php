<?php

namespace SBuilder\SchemaBuilder;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Schema;
use SBuilder\RequestResolver\ResolverInterface;

/**
 * Интерфейс основного строителя схемы GraphQL
 */
interface SchemaBuilderInterface
{
    /**
     * Регистратор запросов для сущностей.
     *
     * Для регистрации первым параметром передается объект GraphQL, описывающий тип сущности.
     * Вторым параметром передается массив запросов, которые необходимо создать для этой сущности.
     * Например:
     * [
     *    SBuilderConstants::LIST_QUERY => new class implements ResolverInterface {...}
     * ]
     *
     * Доступные типы запросов:
     * SBuilderConstants::LIST_QUERY - Листинг сущностей
     * SBuilderConstants::AGGREGATE_QUERY - Аггрегация данных по сущностям
     * SBuilderConstants::INSERT_MUTATION - Мутация вставки сущностей
     * SBuilderConstants::UPDATE_MUTATION - Мутация обновления сущностей
     * SBuilderConstants::DELETE_MUTATION - Мутация удаления сущностей
     *
     * @param ObjectType $object
     * @param ResolverInterface[] $queries
     */
    public function registerEntity(ObjectType $object, array $queries): void;

    /**
     * Регистрация кастомного запроса
     *
     * @param string $queryName
     * @param array $query
     */
    public function registerQuery(string $queryName, array $query): void;

    /**
     * Регистрация кастомной мутации
     *
     * @param string $mutationName
     * @param array $mutation
     */
    public function registerMutation(string $mutationName, array $mutation): void;

    /**
     * Генерация схемы, пригодной для использования в GraphQL эндпоинте
     *
     * @return Schema
     */
    public function buildSchema(): Schema;
}