<?php

namespace SBuilder\RequestResolver;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
interface RequestResolverFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return RequestResolverInterface
     */
    public static function make(?Logger $logger = null): RequestResolverInterface;
}