<?php

namespace SBuilder\RequestResolver;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use Monolog\Logger;
use SBuilder\RequestParser\RequestedFieldsParser\RequestedFieldsParserFactory;
use SBuilder\RequestParser\Types\ResolveParams;

/**
 * Генератор резолверов
 */
class RequestResolver implements RequestResolverInterface
{
    /** @var Logger|null */
    private $logger;

    /**
     * RequestResolver constructor.
     *
     * @param Logger|null $logger
     */
    public function __construct(?Logger $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * Генерация резолвера
     *
     * @param ObjectType $object
     * @param ResolverInterface $resolver
     *
     * @return callable
     */
    public function generate(ObjectType $object, ResolverInterface $resolver): callable
    {
        $logger = $this->logger;
        return function ($objectValue, $args, $context, ResolveInfo $info) use ($logger, &$object, $resolver) {
            $baseParameters = new ResolveParams($objectValue, $args, $context, $info);
            $fields = RequestedFieldsParserFactory::make($logger)->parseRequest($baseParameters);

            return $resolver->resolveQuery($baseParameters, $object, $fields);
        };
    }
}