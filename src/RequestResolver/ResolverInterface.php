<?php

namespace SBuilder\RequestResolver;

use GraphQL\Type\Definition\ObjectType;
use JsonSerializable;
use SBuilder\RequestParser\RequestedFieldsParser\RequestedFieldInterface;
use SBuilder\RequestParser\Types\ResolveParams;
use Throwable;

/**
 * Интерфейс резолвера GraphQL запросов.
 */
interface ResolverInterface
{
    /**
     * Резолвинг запроса GraphQL.
     *
     * На выходе должен получиться либо массив с реузльтатами работы, либо
     * объект, готовый к сериализации.
     *
     * @param ResolveParams $originalParams
     * @param ObjectType $graphQlObject
     * @param RequestedFieldInterface[] $fields
     *
     * @return array|JsonSerializable|null
     * @throws Throwable
     */
    public function resolveQuery(ResolveParams $originalParams, ObjectType $graphQlObject, array $fields);
}