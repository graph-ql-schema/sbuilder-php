<?php

namespace SBuilder\RequestResolver;

use Monolog\Logger;

/**
 * Фабрика сервиса
 */
class RequestResolverFactory implements RequestResolverFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param Logger|null $logger
     * @return RequestResolverInterface
     */
    public static function make(?Logger $logger = null): RequestResolverInterface
    {
        return new RequestResolver($logger);
    }
}