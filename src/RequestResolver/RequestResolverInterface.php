<?php

namespace SBuilder\RequestResolver;

use GraphQL\Type\Definition\ObjectType;

/**
 * Генератор резолверов
 */
interface RequestResolverInterface
{
    /**
     * Генерация резолвера
     *
     * @param ObjectType $object
     * @param ResolverInterface $resolver
     *
     * @return callable
     */
    public function generate(ObjectType $object, ResolverInterface $resolver): callable;
}