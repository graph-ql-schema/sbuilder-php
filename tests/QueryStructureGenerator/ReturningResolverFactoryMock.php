<?php

namespace SBuilder\Tests\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolverFactoryInterface;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolverInterface;

/**
 * Подставка для тестирования
 */
class ReturningResolverFactoryMock implements ReturningResolverFactoryInterface
{
    /**
     * Генерация сервиса
     *
     * @param ObjectType $objectType
     * @return ReturningResolverInterface
     */
    public static function make(ObjectType $objectType): ReturningResolverInterface
    {
        return new ReturningResolverMock();
    }
}