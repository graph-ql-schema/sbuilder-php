<?php

namespace SBuilder\Tests\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\InsertOrUpdateMutationGenerator;
use SBuilder\Tests\Mocks\AliasGenerationServiceMock;

class InsertOrUpdateMutationGeneratorTest extends TestCase
{
    /**
     * Данные для тестирования метода generate
     *
     * @return array
     */
    public function dataForGeneration() {
        return [
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(['id' => Type::int()], "test"),
                    new QueryGeneratorProcessorMock(null, "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // operationType,
                "update",
                // descriptionGenerator
                function () {return "test";},
                // suffix
                "_update",
                // resolver
                function () {},
                // result
                [
                    'type' => new ObjectType([
                        'name' => "user_update_result_type",
                        'fields' => [
                            "affected_rows" => [
                                'type' => Type::nonNull(Type::int()),
                                'description' => "Количество записей, обработанных запросом",
                            ],
                            "returning" => [
                                'type' => Type::listOf(new ObjectType([
                                    "name" => "User",
                                    'description' => 'Our blog visitor',
                                    'fields' => [
                                        'id' => Type::int(),
                                        'email' => Type::string()
                                    ]
                                ])),
                                'description' => "Массив объектов, обработанных в ходе выполнения запроса",
                                'resolve' => [new ReturningResolverMock(), "parse"],
                            ],
                        ],
                        'description' => "Результат обработки значений для сущности 'User'",
                    ]),
                    'name' => "user_update",
                    'description' => "test",
                    'args' => [
                        'test' => ['id' => Type::int()]
                    ],
                    'resolve' => function () {},
                ]
            ],
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(['id' => Type::int()], "test"),
                    new QueryGeneratorProcessorMock(['email' => Type::string()], "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // operationType,
                "update",
                // descriptionGenerator
                function () {return "test";},
                // suffix
                "_update",
                // resolver
                function () {},
                // result
                [
                    'type' => new ObjectType([
                        'name' => "user_update_result_type",
                        'fields' => [
                            "affected_rows" => [
                                'type' => Type::nonNull(Type::int()),
                                'description' => "Количество записей, обработанных запросом",
                            ],
                            "returning" => [
                                'type' => Type::listOf(new ObjectType([
                                    "name" => "User",
                                    'description' => 'Our blog visitor',
                                    'fields' => [
                                        'id' => Type::int(),
                                        'email' => Type::string()
                                    ]
                                ])),
                                'description' => "Массив объектов, обработанных в ходе выполнения запроса",
                                'resolve' => [new ReturningResolverMock(), "parse"],
                            ],
                        ],
                        'description' => "Результат обработки значений для сущности 'User'",
                    ]),
                    'name' => "user_update",
                    'description' => "test",
                    'args' => [
                        'test' => ['id' => Type::int()],
                        'off' => ['email' => Type::string()],
                    ],
                    'resolve' => function () {},
                ]
            ],
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(null, "test"),
                    new QueryGeneratorProcessorMock(null, "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // operationType,
                "update",
                // descriptionGenerator
                function () {return "test";},
                // suffix
                "_update",
                // resolver
                function () {},
                // result
                null
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGeneration
     * @param array $processors
     * @param ObjectType $object
     * @param string $operationType
     * @param callable $descriptionGenerator
     * @param string $suffix
     * @param callable $resolver
     * @param array $result
     */
    public function testForGeneration(
        array $processors,
        ObjectType $object,
        string $operationType,
        callable $descriptionGenerator,
        string $suffix,
        callable $resolver,
        ?array $result
    ) {
        $instance = new InsertOrUpdateMutationGenerator(
            new ReturningResolverFactoryMock(),
            new AliasGenerationServiceMock(),
            $operationType,
            $descriptionGenerator,
            $suffix,
            $processors
        );

        $this->assertEquals($result, $instance->generate($object, $resolver));
    }
}