<?php


namespace SBuilder\Tests\QueryStructureGenerator;


use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\DeleteMutationGenerator;
use SBuilder\Tests\Mocks\AliasGenerationServiceMock;

class DeleteMutationGeneratorTest extends TestCase
{
    /**
     * Данные для тестирования метода generate
     *
     * @return array
     */
    public function dataForGeneration() {
        return [
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(['id' => Type::int()], "test"),
                    new QueryGeneratorProcessorMock(null, "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // resolver
                function () {},
                // result
                [
                    'type' => new ObjectType([
                        'name' => "user_delete_result_type",
                        'fields' => [
                            "affected_rows" => [
                                'type' => Type::nonNull(Type::int()),
                                'description' => "Количество записей, удаленных запросом",
                            ]
                        ],
                        'description' => "Результат удаления значений для сущности 'User'",
                    ]),
                    'name' => "user_delete",
                    'description' => "Мутация удаления значений объекта 'User'",
                    'args' => [
                        'test' => ['id' => Type::int()]
                    ],
                    'resolve' => function () {},
                ]
            ],
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(['id' => Type::int()], "test"),
                    new QueryGeneratorProcessorMock(['email' => Type::string()], "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // resolver
                function () {},
                // result
                [
                    'type' => new ObjectType([
                        'name' => "user_delete_result_type",
                        'fields' => [
                            "affected_rows" => [
                                'type' => Type::nonNull(Type::int()),
                                'description' => "Количество записей, удаленных запросом",
                            ]
                        ],
                        'description' => "Результат удаления значений для сущности 'User'",
                    ]),
                    'name' => "user_delete",
                    'description' => "Мутация удаления значений объекта 'User'",
                    'args' => [
                        'test' => ['id' => Type::int()],
                        'off' => ['email' => Type::string()],
                    ],
                    'resolve' => function () {},
                ]
            ],
            [
                // processors
                [
                    new QueryGeneratorProcessorMock(null, "test"),
                    new QueryGeneratorProcessorMock(null, "off"),
                ],
                // objectType
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                // resolver
                function () {},
                // result
                null
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGeneration
     * @param array $processors
     * @param ObjectType $object
     * @param callable $resolver
     * @param array $result
     */
    public function testForGeneration(array $processors, ObjectType $object, callable $resolver, ?array $result) {
        $instance = new DeleteMutationGenerator(
            $processors,
            new AliasGenerationServiceMock()
        );

        $this->assertEquals($result, $instance->generate($object, $resolver));
    }
}