<?php

namespace SBuilder\Tests\QueryStructureGenerator\GroupByParametersGenerator;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\GroupByParametersGenerator\GroupByParametersGenerator;
use SBuilder\QueryStructureGenerator\GroupByParametersGenerator\GroupByParametersGeneratorFactory;
use SBuilder\QueryStructureGenerator\GroupByParametersGenerator\GroupByParametersGeneratorInterface;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class GroupByParametersGeneratorTest extends TestCase
{
    /**
     * Creates data for testing generate method.
     *
     * @return array[]
     */
    public function generateProvider(): array
    {
        return [
            // General test data.
            [
                // testObject
                new ObjectType([
                    'name' => 'Role',
                    'description' => 'Role entity',
                    'fields' => [
                        'field_1' => [
                            'type' => Type::int(),
                            'name' => 'field_1'
                        ]
                    ]
                ]),
                // expect
                [
                    'type' => Type::listOf(new EnumType([
                        'name' => 'role_group_by_parameters_group_by',
                        'description' => 'Field variants for grouping',
                        'values' => [
                            'field_1' => [
                                'value' => 'field_1',
                                'description' => 'Value of field_1'
                            ]
                        ]
                    ])),
                    'description' => 'Grouping by passed params'
                ],
                // message
                'Should return proper object.'
            ],
            // Test data for when root type of an object is ObjectType.
            [
                // testObject
                new ObjectType([
                    'name' => 'Role',
                    'description' => 'Role entity',
                    'fields' => [
                        'field_1' => [
                            'type' => new ObjectType([
                                'name' => 'Object Type',
                                'description' => 'Object type description'
                            ]),
                            'name' => 'field_1'
                        ]
                    ]
                ]),
                // expect
                [
                    'type' => Type::listOf(new EnumType([
                        'name' => 'role_group_by_parameters_group_by',
                        'description' => 'Field variants for grouping',
                        'values' => []
                    ])),
                    'description' => 'Grouping by passed params'
                ],
                // message
                'Should return an object with empty values field.'
            ]
        ];
    }

    /**
     * Test generate method.
     *
     * @dataProvider generateProvider
     * @param ObjectType $testObject
     * @param array $expect
     * @param string $message
     */
    public function testGenerate(ObjectType $testObject, array $expect, string $message)
    {
        // Test that logging works but without producing any input.
        $logger = new Logger('group_by_parameters_generator_test_logger');
        $logger->pushHandler(new NullHandler());

        $instance = new GroupByParametersGenerator(new GraphQlRootTypeGetterMock(), $logger);
        $result = $instance->generate($testObject, 'role');
        $this->assertEquals($expect, $result, $message);
    }

    /**
     * Test factory.
     */
    public function testFactory()
    {
        // Test that logging works but without producing any input.
        $logger = new Logger('group_by_parameters_generator_test_logger');
        $logger->pushHandler(new NullHandler());

        $this->assertTrue(
            GroupByParametersGeneratorFactory::create($logger) instanceof GroupByParametersGeneratorInterface,
            "Returned result is not instance of GroupByParametersGeneratorInterface"
        );
    }
}
