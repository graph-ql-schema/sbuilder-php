<?php

namespace SBuilder\Tests\QueryStructureGenerator\AliasGenerationService;

use LogicException;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationService;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceFactory;
use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;

class AliasGenerationServiceTest extends TestCase
{

    /**
     * Creates data for testing generate method.
     *
     * @return array[]
     */
    public function generateProvider(): array
    {
        return [
            [["а" => "i", "б" => "b", "-" => "_", " " => "_"], '', ''],
            [["а" => "i", "б" => "b", "-" => "_", " " => "_"], 'ааа', 'iii'],
            [["а" => "i", "б" => "b", "-" => "_", " " => "_"], 'абВ_а', 'ibi'],
        ];
    }

    /**
     * Testing generate method using data from generateProvider.
     *
     * @dataProvider generateProvider
     * @param array $map
     * @param string $source
     * @param string $expect
     */
    public function testGenerate(array $map, string $source, string $expect)
    {
        $instance = new AliasGenerationService($map);
        $testResult = $instance->generate($source);
        $this->assertSame($expect, $testResult, sprintf('generate result %s is not equal to %s.', $testResult, $expect));
    }

    /**
     * Testing exception for bad regex pattern.
     */
    public function testGenerateLogicException()
    {
        $instance = new AliasGenerationService(["[" => "_"]);
        $this->expectException(LogicException::class);
        $instance->generate('');
    }

    /**
     * Testing factory method.
     */
    public function testGenerationServiceFactory()
    {
        $factory = new AliasGenerationServiceFactory();
        $instance = $factory->create();
        $this->assertEquals(
            true,
            $this
                ->isInstanceOf(AliasGenerationServiceInterface::class)
                ->evaluate($instance, '', true),
            'Not an AliasGenerationServiceInterface instance.'
        );
    }
}
