<?php

namespace SBuilder\Tests\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\GenerationProcessors\QueryGeneratorProcessorInterface;

/**
 * Подставка для тестирования
 */
class QueryGeneratorProcessorMock implements QueryGeneratorProcessorInterface
{
    /** @var array|null */
    private $result;

    /** @var string */
    private $name;

    /**
     * QueryGeneratorProcessorMock constructor.
     * @param array|null $result
     * @param string $name
     */
    public function __construct(?array $result, string $name = "test")
    {
        $this->result = $result;
        $this->name = $name;
    }

    /**
     * Получение названия генерируемого поля
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Генерация
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @return array|null
     */
    public function generate(ObjectType $object, string $namePrefix): ?array
    {
        return $this->result;
    }
}