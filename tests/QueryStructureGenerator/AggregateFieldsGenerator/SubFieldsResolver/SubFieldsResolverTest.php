<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver;

use DateTime;
use LogicException;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver\SubFieldsResolver;

class SubFieldsResolverTest extends TestCase
{
    /**
     * Набор данных для тестирования метода resolve
     *
     * @return array
     */
    public function dataForResolve() {
        return [
            ["test", ["test" => [1, 2, 3]], [1, 2, 3]],
            ["test", ["test" => [2, 2, 2]], [2]],
            ["test", null, null],
            ["test1", ["test" => [2, 2, 2]], null],
            [
                "test",
                [
                    "test" => [
                        DateTime::createFromFormat(DATE_RFC3339, "2020-01-01T00:00:00Z"),
                        DateTime::createFromFormat(DATE_RFC3339, "2020-01-01T00:00:00Z"),
                        DateTime::createFromFormat(DATE_RFC3339, "2020-01-01T00:00:00Z"),
                    ],
                ],
                [DateTime::createFromFormat(DATE_RFC3339, "2020-01-01T00:00:00Z")],
            ],
        ];
    }

    /**
     * Тестирование метода resolve
     *
     * @dataProvider dataForResolve
     * @param string $fieldCode
     * @param $data
     * @param $result
     */
    public function testResolve(string $fieldCode, $data, $result) {
        $instance = new SubFieldsResolver($fieldCode);

        $this->assertEquals($result, $instance->resolve($data));
    }

    /**
     * Набор данных для тестирования ошибок метода resolve
     *
     * @return array
     */
    public function dataForResolveErrors() {
        return [
            ["test", "test"],
        ];
    }

    /**
     * Тестирование ошибок парсинга методом resolve
     *
     * @dataProvider dataForResolveErrors
     * @expectedException LogicException
     * @param string $fieldCode
     * @param $data
     */
    public function testResolveError(string $fieldCode, $data) {
        $instance = new SubFieldsResolver($fieldCode);
        $instance->resolve($data);
    }
}