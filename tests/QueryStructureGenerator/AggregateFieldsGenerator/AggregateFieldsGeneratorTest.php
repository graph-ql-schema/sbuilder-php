<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGenerator;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ProcessResult;

class AggregateFieldsGeneratorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    new AggregateFieldsGeneratorProcessorMock(null),
                    new AggregateFieldsGeneratorProcessorMock(new ProcessResult(['type' => Type::int()], SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY)),
                ],
                new ObjectType([
                    'name' => 'user_aggregation',
                    'fields' => [
                        SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY => ['type' => Type::int()],
                    ],
                    'description' => "Сущность для запросов аггрегации для объекта: 'User'",
                ])
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    new AggregateFieldsGeneratorProcessorMock(new ProcessResult(['type' => Type::string()], SBuilderConstants::COUNT_OPERATION_SCHEMA_KEY)),
                    new AggregateFieldsGeneratorProcessorMock(new ProcessResult(['type' => Type::int()], SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY)),
                ],
                new ObjectType([
                    'name' => 'user_aggregation',
                    'fields' => [
                        SBuilderConstants::COUNT_OPERATION_SCHEMA_KEY => ['type' => Type::string()],
                        SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY => ['type' => Type::int()],
                    ],
                    'description' => "Сущность для запросов аггрегации для объекта: 'User'",
                ])
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param ObjectType $baseObject
     * @param array $processors
     * @param ObjectType $result
     */
    public function testGenerate(ObjectType $baseObject, array $processors, ObjectType $result) {
        $instance = new AggregateFieldsGenerator($processors, null);

        $this->assertEquals($result, $instance->generate($baseObject));
    }
}