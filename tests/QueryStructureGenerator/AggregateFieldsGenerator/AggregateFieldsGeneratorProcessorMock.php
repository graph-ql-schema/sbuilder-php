<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\AggregateFieldsGeneratorProcessorInterface;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ProcessResult;

/**
 * Подставка для тестирования
 */
class AggregateFieldsGeneratorProcessorMock implements AggregateFieldsGeneratorProcessorInterface
{
    /** @var ?ProcessResult */
    private $result;

    /**
     * AggregateFieldsGeneratorProcessorMock constructor.
     *
     * @param $result
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * Генерация поля для сущности
     *
     * @param ObjectType $object
     * @return ProcessResult
     */
    public function generateField(ObjectType $object): ?ProcessResult
    {
        return $this->result;
    }
}