<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use PHPUnit\Framework\TestCase;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\CountAggregateFieldsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ProcessResult;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class CountAggregateFieldsGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generateField
     *
     * @return array
     */
    public function dataForGenerateField()
    {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'testUnion' => new UnionType([
                            'name' => 'testUnionType',
                            'types' => [
                                new ObjectType([
                                    'name' => 'unionObjectType1',
                                    'fields' => [
                                        'firstName' => Type::string()
                                    ]
                                ]),
                                new ObjectType([
                                    'name' => 'unionObjectType2',
                                    'fields' => [
                                        'lastName' => Type::string()
                                    ]
                                ])
                            ]
                        ])
                    ]
                ]),
                new ProcessResult(
                    [
                        "type" => Type::int(),
                        "description" => "Получение количества элементов сущности во всей таблице или в каждой группе отдельно, при наличии GroupBy для сущности: 'User'",
                    ],
                    SBuilderConstants::COUNT_OPERATION_SCHEMA_KEY
                ),
            ],
            [
                new ObjectType([
                    "name" => "TestForUnion",
                    'fields' => [
                        'testUnion' => new UnionType([
                            'name' => 'testUnionType',
                            'types' => [
                                new ObjectType([
                                    'name' => 'unionObjectType1',
                                    'fields' => [
                                        'firstName' => Type::string()
                                    ]
                                ]),
                                new ObjectType([
                                    'name' => 'unionObjectType2',
                                    'fields' => [
                                        'lastName' => Type::string()
                                    ]
                                ])
                            ]
                        ])
                    ]
                ]),
                null
            ],
            [
                new ObjectType([
                    "name" => "TestForObjectType",
                    'fields' => [
                        'testObjectType' => new ObjectType([
                            'name' => 'testingObjectType',
                            'fields' => [
                                'firstName' => Type::string()
                            ]
                        ])
                    ]
                ]),
                null
            ]
        ];
    }

    /**
     * Тестирование метода generateField
     *
     * @dataProvider dataForGenerateField
     * @param ObjectType $object
     * @param ProcessResult|null $result
     */
    public function testGenerateField(ObjectType $object, ?ProcessResult $result) {
        $instance = new CountAggregateFieldsGeneratorProcessor(
            new GraphQlRootTypeGetterMock(),
            null
        );

        $this->assertEquals($result, $instance->generateField($object));
    }
}
