<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\SubFieldsResolver\SubFieldsResolverFactoryInterface;

/**
 * Подставка для тестирования
 */
class SubFieldsResolverFactoryMock implements SubFieldsResolverFactoryInterface
{
    /**
     * Фабричный метод генерации callback функции для резолвинга поля для аггрегирующей сущности
     *
     * @param string $fieldCode
     * @return callable
     */
    public static function make(string $fieldCode): callable
    {
        return function () {};
    }
}