<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use PHPUnit\Framework\TestCase;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ProcessResult;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\VariantsAggregateFieldsGeneratorProcessor;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class VariantsAggregateFieldsGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generateField
     *
     * @return array
     */
    public function dataForGenerateField() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'testUnion' => new UnionType([
                            'name' => 'testUnionType',
                            'types' => [
                                new ObjectType([
                                    'name' => 'unionObjectType1',
                                    'fields' => [
                                        'firstName' => Type::string()
                                    ]
                                ]),
                                new ObjectType([
                                    'name' => 'unionObjectType2',
                                    'fields' => [
                                        'lastName' => Type::string()
                                    ]
                                ])
                            ]
                        ])
                    ]
                ]),
                new ProcessResult(
                    [
                        "type" => Type::nonNull(new ObjectType([
                            'name' => "user_aggregate_variants_field_response_type",
                            'fields' => [
                                'id' => [
                                    'type' => Type::listOf(Type::int()),
                                    'description' => "Получение списка уникальных значений для поля id",
                                    'resolve' => function () {},
                                ],
                                'email' => [
                                    'type' => Type::listOf(Type::string()),
                                    'description' => "Получение списка уникальных значений для поля email",
                                    'resolve' => function () {},
                                ],
                            ],
                            'description' => "Доступный для выбора список полей для получения уникальных значений по ним для сущности 'User'",
                        ])),
                        "description" => "Получение списка уникальных значений для полей сущности 'User'",
                    ],
                    SBuilderConstants::VARIANTS_OPERATION_SCHEMA_KEY
                ),
            ],
        ];
    }

    /**
     * Тестирование метода generateField
     *
     * @dataProvider dataForGenerateField
     * @param ObjectType $object
     * @param ProcessResult $result
     */
    public function testGenerateField(ObjectType $object, ProcessResult $result) {
        $instance = new VariantsAggregateFieldsGeneratorProcessor(
            new GraphQlRootTypeGetterMock(),
            new SubFieldsResolverFactoryMock(),
            null
        );

        $this->assertEquals($result, $instance->generateField($object));
    }
}
