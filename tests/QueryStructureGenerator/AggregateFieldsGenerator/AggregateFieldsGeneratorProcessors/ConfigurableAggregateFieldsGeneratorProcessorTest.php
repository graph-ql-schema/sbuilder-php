<?php

namespace SBuilder\Tests\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors;

use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ConfigurableAggregateFieldsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorProcessors\ProcessResult;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class ConfigurableAggregateFieldsGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования генерации аггрегации
     *
     * @return array
     */
    public function dataForGenerateFieldTest() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                array(
                    "typeGetter" => new GraphQlRootTypeGetterMock(),
                    "fieldResolverFactory" => new SubFieldsResolverFactoryMock(),
                    "operatorType" => "min",
                    "availableTypeClasses" => [
                        IntType::class,
                        FloatType::class,
                    ],
                    "operatorDescription" => function (ObjectType $object): string {
                        return "Description for $object->name";
                    },
                    "valueDescriptionGenerator" => function (string $code, ObjectType $object): string {
                        return "Description for $code in $object->name";
                    },
                ),
                new ProcessResult(
                    [
                        "type" => Type::nonNull(new ObjectType([
                            "name" => "user_aggregate_min_field_response_type",
                            "fields" => [
                                "id" => [
                                    'type' => Type::float(),
                                    'description' => "Description for id in User",
                                    'resolve' => function () {}
                                ]
                            ],
                            "description" => "Description for User",
                        ])),
                        "description" => "Description for User",
                    ],
                    "min"
                ),
            ],
        ];
    }

    /**
     * Тестирование генерации конфигурации при помощи процессора
     *
     * @dataProvider dataForGenerateFieldTest
     * @param ObjectType $object
     * @param array $fields
     * @param ProcessResult $result
     */
    public function testGenerateField(ObjectType $object, array $fields, ProcessResult $result) {
        $instance = new ConfigurableAggregateFieldsGeneratorProcessor(
            $fields["typeGetter"],
            $fields["fieldResolverFactory"],
            $fields["operatorType"],
            $fields["availableTypeClasses"],
            $fields["operatorDescription"],
            $fields["valueDescriptionGenerator"],
            null
        );

        $this->assertEquals($result, $instance->generateField($object));
    }
}