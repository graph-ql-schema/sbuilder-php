<?php

namespace SBuilder\Tests\QueryStructureGenerator;

use LogicException;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolverInterface;

/**
 * Подставка для тестирования
 */
class ReturningResolverMock implements ReturningResolverInterface
{
    /**
     * Резолвинг поля returning для обновления/создания сущности
     *
     * @param $parentData
     * @return mixed
     * @throws LogicException
     */
    public function parse($parentData)
    {
        return null;
    }
}