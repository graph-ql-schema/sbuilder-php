<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

/**
 * Подставка для тестирования
 */
class WhereParametersOperatorGeneratorMock implements WhereParametersOperatorGeneratorInterface
{
    /** @var array|null */
    private $result;

    /**
     * WhereParametersOperatorGeneratorMock constructor.
     *
     * @param array|null $result
     */
    public function __construct(?array $result)
    {
        $this->result = $result;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array
     */
    public function generateOperations(ObjectType $object, string $namePrefix, int $level): ?array
    {
        return $this->result;
    }
}