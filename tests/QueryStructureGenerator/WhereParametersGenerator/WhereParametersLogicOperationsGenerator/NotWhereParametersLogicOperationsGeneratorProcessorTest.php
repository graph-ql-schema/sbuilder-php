<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\NotWhereParametersLogicOperationsGeneratorProcessor;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

class NotWhereParametersLogicOperationsGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                new WhereParametersOperatorGeneratorMock(null),
                "test",
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => []
                ]),
                new WhereParametersOperatorGeneratorMock(['id' => Type::int()]),
                "test",
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                new WhereParametersOperatorGeneratorMock(['id' => Type::int()]),
                "test",
                [
                    "description" => "Оператор NOT для объединения условий",
                    "type" => new InputObjectType([
                        "name" => "test_not",
                        "fields" => ['id' => Type::int()],
                        "description" => "Объект листинга для оператора NOT для объединения условий",
                    ]),
                ],
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                new WhereParametersOperatorGeneratorMock(['id' => Type::int()]),
                "",
                [
                    "description" => "Оператор NOT для объединения условий",
                    "type" => new InputObjectType([
                        "name" => "not",
                        "fields" => ['id' => Type::int()],
                        "description" => "Объект листинга для оператора NOT для объединения условий",
                    ]),
                ],
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param ObjectType $object
     * @param WhereParametersOperatorGeneratorInterface $generator
     * @param string $namePrefix
     * @param array|null $result
     */
    public function testGenerate(
        ObjectType $object,
        WhereParametersOperatorGeneratorInterface $generator,
        string $namePrefix,
        ?array  $result
    ) {
        $instance = new NotWhereParametersLogicOperationsGeneratorProcessor($generator, null);

        $this->assertEquals($result, $instance->generate($object, 0, $namePrefix));
    }
}