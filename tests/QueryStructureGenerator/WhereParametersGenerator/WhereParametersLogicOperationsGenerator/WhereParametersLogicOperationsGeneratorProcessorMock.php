<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorProcessorInterface;

/**
 * Подставка для тестирования
 */
class WhereParametersLogicOperationsGeneratorProcessorMock implements WhereParametersLogicOperationsGeneratorProcessorInterface
{
    /** @var array|null */
    private $result;

    /** @var string */
    private $operationCode;

    /** @var WhereParametersGeneratorInterface */
    public $whereParametersGenerator;

    /**
     * WhereParametersLogicOperationsGeneratorProcessorMock constructor.
     *
     * @param array|null $result
     * @param string $operationCode
     */
    public function __construct(?array $result, string $operationCode)
    {
        $this->result = $result;
        $this->operationCode = $operationCode;
    }

    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void
    {
        $this->whereParametersGenerator = $whereParametersGenerator;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(ObjectType $object, int $level, string $namePrefix): ?array
    {
        return $this->result;
    }

    /**
     * Код операции, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operationCode(): string
    {
        return $this->operationCode;
    }
}