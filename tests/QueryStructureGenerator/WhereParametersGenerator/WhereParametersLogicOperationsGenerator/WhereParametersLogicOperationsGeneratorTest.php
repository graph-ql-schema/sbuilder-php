<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGenerator;

class WhereParametersLogicOperationsGeneratorTest extends TestCase
{
    /**
     * Тестирование метода setWhereParametersOperatorGenerator
     */
    public function testSetWhereParametersOperatorGenerator() {
        $generator = new WhereParametersGeneratorMock(null);
        $processors = [
            new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
            new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
            new WhereParametersLogicOperationsGeneratorProcessorMock(null, "")
        ];

        $instance = new WhereParametersLogicOperationsGenerator(...$processors);
        $instance->setWhereParametersOperatorGenerator($generator);

        /** @var WhereParametersLogicOperationsGeneratorProcessorMock $processor */
        foreach ($processors as $processor) {
            $this->assertEquals($generator, $processor->whereParametersGenerator);
        }
    }

    /**
     * Набор данных для тестирования метода generateParametersForObject
     *
     * @return array
     */
    public function dataForGenerateParametersForObject() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'fields' => [
                        'id' => Type::int()
                    ]
                ]),
                [
                    new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
                ],
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'fields' => [
                        'id' => Type::int()
                    ]
                ]),
                [
                    new WhereParametersLogicOperationsGeneratorProcessorMock(['type' => Type::int()], "and"),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(null, ""),
                ],
                [
                    'and' => ['type' => Type::int()],
                ],
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'fields' => [
                        'id' => Type::int()
                    ]
                ]),
                [
                    new WhereParametersLogicOperationsGeneratorProcessorMock(['type' => Type::int()], "and"),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(['type' => Type::string()], "or"),
                    new WhereParametersLogicOperationsGeneratorProcessorMock(['type' => Type::id()], "not"),
                ],
                [
                    'and' => ['type' => Type::int()],
                    'or' => ['type' => Type::string()],
                    'not' => ['type' => Type::id()],
                ],
            ],
        ];
    }

    /**
     * Тестирование метода generateParametersForObject
     *
     * @dataProvider dataForGenerateParametersForObject
     * @param ObjectType $object
     * @param array $processors
     * @param array|null $result
     */
    public function testGenerateParametersForObject(ObjectType $object, array $processors, ?array $result)
    {
        $instance = new WhereParametersLogicOperationsGenerator(...$processors);
        $testResult = $instance->generateParametersForObject($object, 0, "");

        $this->assertEquals($result, $testResult);
    }
}