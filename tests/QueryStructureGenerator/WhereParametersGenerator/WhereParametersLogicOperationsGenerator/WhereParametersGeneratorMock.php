<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

/**
 * Подставка для тестирования
 */
class WhereParametersGeneratorMock implements WhereParametersGeneratorInterface
{
    /** @var array|null */
    private $result;

    /**
     * WhereParametersGeneratorMock constructor.
     * @param array|null $result
     */
    public function __construct(?array $result)
    {
        $this->result = $result;
    }

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param string $namePrefix
     * @param int $level
     * @return array|null
     */
    public function buildParameters(ObjectType $object, string $namePrefix, int $level): ?array
    {
        return $this->result;
    }
}