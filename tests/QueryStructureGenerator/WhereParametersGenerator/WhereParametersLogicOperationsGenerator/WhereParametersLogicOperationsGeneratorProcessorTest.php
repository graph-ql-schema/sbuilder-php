<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorProcessor;

class WhereParametersLogicOperationsGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    "generator" => new WhereParametersGeneratorMock(null),
                    "maxLevel" => 10,
                    "operationName" => "and",
                    "operationCode" => "_and",
                ],
                "test",
                0,
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    "generator" => new WhereParametersGeneratorMock(['id' => Type::int()]),
                    "maxLevel" => 10,
                    "operationName" => "and",
                    "operationCode" => "_and",
                ],
                "test",
                20,
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => []
                ]),
                [
                    "generator" => new WhereParametersGeneratorMock(['id' => Type::int()]),
                    "maxLevel" => 10,
                    "operationName" => "and",
                    "operationCode" => "_and",
                ],
                "test",
                0,
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    "generator" => new WhereParametersGeneratorMock(['id' => Type::int()]),
                    "maxLevel" => 10,
                    "operationName" => "and",
                    "operationCode" => "_and",
                ],
                "test",
                0,
                [
                    "description" => "Оператор and для объединения условий",
                    "type" => Type::listOf(Type::nonNull(
                        new InputObjectType([
                            "name" => "test_and",
                            "fields" => ['id' => Type::int()],
                            "description" => "Объект листинга для оператора and для объединения условий",
                        ])
                    )),
                ]
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                [
                    "generator" => new WhereParametersGeneratorMock(['id' => Type::int()]),
                    "maxLevel" => 10,
                    "operationName" => "and",
                    "operationCode" => "_and",
                ],
                "",
                0,
                [
                    "description" => "Оператор and для объединения условий",
                    "type" => Type::listOf(Type::nonNull(
                        new InputObjectType([
                            "name" => "and",
                            "fields" => ['id' => Type::int()],
                            "description" => "Объект листинга для оператора and для объединения условий",
                        ])
                    )),
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param ObjectType $object
     * @param array $fields
     * @param string $namePrefix
     * @param int $level
     * @param array|null $result
     */
    public function testGenerate(
        ObjectType $object,
        array $fields,
        string $namePrefix,
        int $level,
        ?array $result
    ) {
        $instance = new WhereParametersLogicOperationsGeneratorProcessor(
            $fields["maxLevel"],
            $fields["operationName"],
            $fields["operationCode"],
            null
        );

        $instance->setWhereParametersOperatorGenerator($fields["generator"]);
        $this->assertEquals($result, $instance->generate($object, $level, $namePrefix));
    }
}