<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorInterface;

/**
 * Подставка для тестирования
 */
class WhereParametersLogicOperationsGeneratorMock implements WhereParametersLogicOperationsGeneratorInterface
{
    /** @var array|null */
    private $result;

    /**
     * WhereParametersLogicOperationsGeneratorMock constructor.
     *
     * @param array|null $result
     */
    public function __construct(?array $result)
    {
        $this->result = $result;
    }

    /**
     * Установка генератора параметров для процессора
     *
     * @param WhereParametersGeneratorInterface $whereParametersGenerator
     */
    public function setWhereParametersOperatorGenerator(WhereParametersGeneratorInterface $whereParametersGenerator): void {}

    /**
     * Генерация параметров
     *
     * @param ObjectType $object
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generateParametersForObject(ObjectType $object, int $level, string $namePrefix): ?array
    {
        return $this->result;
    }
}