<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersGenerator;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersLogicOperationsGenerator\WhereParametersLogicOperationsGeneratorInterface;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorInterface;

class WhereParametersGeneratorTest extends TestCase
{
    /**
     * Данные для тестирования метода buildParameters
     */
    public function dataForBuildParameters() {
        return [
            [
                new WhereParametersLogicOperationsGeneratorMock(null),
                new WhereParametersOperatorGeneratorMock(null),
                null,
            ],
            [
                new WhereParametersLogicOperationsGeneratorMock(['id' => Type::int()]),
                new WhereParametersOperatorGeneratorMock(null),
                ['id' => Type::int()],
            ],
            [
                new WhereParametersLogicOperationsGeneratorMock(['id' => Type::int()]),
                new WhereParametersOperatorGeneratorMock(['email' => Type::string()]),
                ['id' => Type::int(), 'email' => Type::string()],
            ],
            [
                new WhereParametersLogicOperationsGeneratorMock(null),
                new WhereParametersOperatorGeneratorMock(['email' => Type::string()]),
                ['email' => Type::string()],
            ],
        ];
    }

    /**
     * Тестирование метода buildParameters
     *
     * @dataProvider dataForBuildParameters
     * @param WhereParametersLogicOperationsGeneratorInterface $logicOperationsGenerator
     * @param WhereParametersOperatorGeneratorInterface $operatorGenerator
     * @param array|null $result
     */
    public function testForBuildParameters(
        WhereParametersLogicOperationsGeneratorInterface $logicOperationsGenerator,
        WhereParametersOperatorGeneratorInterface $operatorGenerator,
        ?array $result
    ) {
        $instance = new WhereParametersGenerator($logicOperationsGenerator, $operatorGenerator, null);

        $this->assertEquals($result, $instance->buildParameters(
            new ObjectType([
                "name" => "User",
                'description' => 'Our blog visitor',
                'fields' => [
                    'id' => Type::int(),
                    'email' => Type::string()
                ]
            ]),
            "",
            0
        ));
    }
}