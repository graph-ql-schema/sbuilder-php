<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\EqualsWhereParametersOperatorGeneratorProcessor;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class EqualsWhereParametersOperatorGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                "test",
                [
                    'type' => Type::int(),
                    'description' => "Операция строго сравнения значения поля с переданным",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param FieldDefinition $field
     * @param string $namePrefix
     * @param array $result
     */
    public function testForGenerate(FieldDefinition $field, string $namePrefix, array $result) {
        $instance = new EqualsWhereParametersOperatorGeneratorProcessor(
            0, new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals($result, $instance->generate($field, 0, $namePrefix));
    }
}