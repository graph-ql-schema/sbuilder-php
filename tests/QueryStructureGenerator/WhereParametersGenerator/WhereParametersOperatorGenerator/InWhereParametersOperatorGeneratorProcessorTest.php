<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\InWhereParametersOperatorGeneratorProcessor;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class InWhereParametersOperatorGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                [
                    'type' => Type::listOf(Type::int()),
                    'description' => "Операция множественного сравнения значения поля с переданным",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param FieldDefinition $field
     * @param array $result
     */
    public function testForGenerate(FieldDefinition $field, array $result) {
        $instance = new InWhereParametersOperatorGeneratorProcessor(
            [], 0, new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals($result, $instance->generate($field, 0, "test"));
    }
}