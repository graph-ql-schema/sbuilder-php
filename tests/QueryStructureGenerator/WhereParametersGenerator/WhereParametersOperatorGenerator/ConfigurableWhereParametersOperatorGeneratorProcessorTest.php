<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\ConfigurableWhereParametersOperatorGeneratorProcessor;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class ConfigurableWhereParametersOperatorGeneratorProcessorTest extends TestCase
{
    /**
     * Тестирование метода operatorCode
     */
    public function testForOperatorCode() {
        $instance = new ConfigurableWhereParametersOperatorGeneratorProcessor(
            [], 0, "test", new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals("test", $instance->operatorCode());
    }

    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                "and",
                "test",
                [
                    'type' => Type::int(),
                    'description' => "Операция and со значением поля",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param FieldDefinition $field
     * @param string $operation
     * @param string $namePrefix
     * @param array $result
     */
    public function testForGenerate(FieldDefinition $field, string $operation, string $namePrefix, array $result) {
        $instance = new ConfigurableWhereParametersOperatorGeneratorProcessor(
            [], 0, $operation, new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals($result, $instance->generate($field, 0, $namePrefix));
    }
}