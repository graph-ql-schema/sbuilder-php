<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGenerator;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class WhereParametersOperatorGeneratorTest extends TestCase
{
    /**
     * Набор данных для тестирования generateOperations
     *
     * @return array
     */
    public function dataForGenerateOperations() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'test' => new ObjectType([
                            'name' => 'data',
                            'fields' => [
                                'id' => Type::int(),
                            ],
                        ])
                    ]
                ]),
                [
                    new WhereParametersOperatorGeneratorProcessorMock(false, "and", ['type' => Type::int()]),
                    new WhereParametersOperatorGeneratorProcessorMock(false, "or", ['type' => Type::string()]),
                ],
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'test' => new ObjectType([
                            'name' => 'data',
                            'fields' => [
                                'id' => Type::int(),
                            ],
                        ])
                    ]
                ]),
                [
                    new WhereParametersOperatorGeneratorProcessorMock(true, "and", ['type' => Type::int()]),
                    new WhereParametersOperatorGeneratorProcessorMock(false, "or", ['type' => Type::string()]),
                ],
                [
                    'id' => [
                        'type' => new InputObjectType([
                            'name' => "test_id",
                            'fields' => [
                                'and' => ['type' => Type::int()],
                            ],
                            'description' => "Фильтр по полю 'id' для сущности",
                        ]),
                        'description' => "Фильтр по полю 'id' для сущности",
                    ],
                    'email' => [
                        'type' => new InputObjectType([
                            'name' => "test_email",
                            'fields' => [
                                'and' => ['type' => Type::int()],
                            ],
                            'description' => "Фильтр по полю 'email' для сущности",
                        ]),
                        'description' => "Фильтр по полю 'email' для сущности",
                    ],
                ],
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'test' => new ObjectType([
                            'name' => 'data',
                            'fields' => [
                                'id' => Type::int(),
                            ],
                        ])
                    ]
                ]),
                [
                    new WhereParametersOperatorGeneratorProcessorMock(true, "and", ['type' => Type::int()]),
                    new WhereParametersOperatorGeneratorProcessorMock(true, "or", ['type' => Type::string()]),
                ],
                [
                    'id' => [
                        'type' => new InputObjectType([
                            'name' => "test_id",
                            'fields' => [
                                'and' => ['type' => Type::int()],
                                'or' => ['type' => Type::string()],
                            ],
                            'description' => "Фильтр по полю 'id' для сущности",
                        ]),
                        'description' => "Фильтр по полю 'id' для сущности",
                    ],
                    'email' => [
                        'type' => new InputObjectType([
                            'name' => "test_email",
                            'fields' => [
                                'and' => ['type' => Type::int()],
                                'or' => ['type' => Type::string()],
                            ],
                            'description' => "Фильтр по полю 'email' для сущности",
                        ]),
                        'description' => "Фильтр по полю 'email' для сущности",
                    ],
                ],
            ],
        ];
    }

    /**
     * Тестирование метода generateOperations
     *
     * @dataProvider dataForGenerateOperations
     * @param ObjectType $object
     * @param array $processors
     * @param array|null $result
     */
    public function testForGenerateOperations(ObjectType $object, array $processors, ?array $result) {
        $instance = new WhereParametersOperatorGenerator($processors, new GraphQlRootTypeGetterMock(), null);

        $this->assertEquals($result, $instance->generateOperations($object, "test", 0));
    }
}