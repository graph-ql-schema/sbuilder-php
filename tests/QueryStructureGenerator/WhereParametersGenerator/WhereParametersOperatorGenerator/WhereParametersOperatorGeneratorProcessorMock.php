<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\WhereParametersOperatorGeneratorProcessorInterface;

/**
 * Подставка для тестирования
 */
class WhereParametersOperatorGeneratorProcessorMock implements WhereParametersOperatorGeneratorProcessorInterface
{
    /** @var bool */
    private $available;

    /** @var string */
    private $code;

    /** @var array */
    private $result;

    /**
     * WhereParametersOperatorGeneratorProcessorMock constructor.
     *
     * @param bool $available
     * @param string $code
     * @param array $result
     */
    public function __construct(bool $available, string $code, array $result)
    {
        $this->available = $available;
        $this->code = $code;
        $this->result = $result;
    }

    /**
     * Проверка доступности процессора
     *
     * @param FieldDefinition $field
     * @param int $level
     * @return bool
     */
    public function isAvailable(FieldDefinition $field, int $level): bool
    {
        return $this->available;
    }

    /**
     * Код оператора, для вставки в схему GraphQL
     *
     * @return string
     */
    public function operatorCode(): string
    {
        return $this->code;
    }

    /**
     * Генерация параметров
     *
     * @param FieldDefinition $field
     * @param int $level
     * @param string $namePrefix
     * @return array
     */
    public function generate(FieldDefinition $field, int $level, string $namePrefix): array
    {
        return $this->result;
    }
}