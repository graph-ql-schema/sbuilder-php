<?php

namespace SBuilder\Tests\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator;

use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\FloatType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\IntType;
use GraphQL\Type\Definition\StringType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\WhereParametersOperatorGenerator\BetweenWhereParametersOperatorGeneratorProcessor;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class BetweenWhereParametersOperatorGeneratorProcessorTest extends TestCase
{
    /**
     * Наборы данных для тестирования метода isAvailable
     *
     * @return array
     */
    public function dataForIsAvailable() {
        return [
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                [
                    IntType::class,
                    FloatType::class,
                    StringType::class,
                ],
                10,
                true,
            ],
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::id(),
                ]),
                [
                    IntType::class,
                    FloatType::class,
                    StringType::class,
                ],
                10,
                false,
            ],
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                [
                    IntType::class,
                    FloatType::class,
                    StringType::class,
                ],
                0,
                false,
            ],
        ];
    }

    /**
     * Тестирование метода isAvailable
     *
     * @dataProvider dataForIsAvailable
     * @param FieldDefinition $field
     * @param array $availableTypes
     * @param int $maxLevel
     * @param bool $result
     */
    public function testForIsAvailable(FieldDefinition $field, array $availableTypes, int $maxLevel, bool $result) {
        $instance = new BetweenWhereParametersOperatorGeneratorProcessor(
            $availableTypes, $maxLevel, new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals($result, $instance->isAvailable($field, 1));
    }

    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                FieldDefinition::create([
                    'name' => 'test',
                    'type' => Type::int(),
                ]),
                "test",
                [
                    'type' => new InputObjectType([
                        'name' => "test_between",
                        'fields' => [
                            '_from' => [
                                'type' => Type::int(),
                                'description' => "Параметр от",
                            ],
                            '_to' => [
                                'type' => Type::int(),
                                'description' => "Параметр до",
                            ],
                        ],
                        'description' => ''
                    ]),
                    'description' => "Операция сравнения значения в формате: 'между двух значений'",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGenerate
     * @param FieldDefinition $field
     * @param string $namePrefix
     * @param array $result
     */
    public function testForGenerate(FieldDefinition $field, string $namePrefix, array $result) {
        $instance = new BetweenWhereParametersOperatorGeneratorProcessor(
            [], 0, new GraphQlRootTypeGetterMock(), null
        );

        $this->assertEquals($result, $instance->generate($field, 0, $namePrefix));
    }
}