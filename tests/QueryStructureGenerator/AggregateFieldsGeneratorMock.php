<?php

namespace SBuilder\Tests\QueryStructureGenerator;

use GraphQL\Type\Definition\ObjectType;
use SBuilder\QueryStructureGenerator\AggregateFieldsGenerator\AggregateFieldsGeneratorInterface;

/**
 * Подставка для тестирования
 */
class AggregateFieldsGeneratorMock implements AggregateFieldsGeneratorInterface
{
    /**
     * Генерация сущности
     *
     * @param ObjectType $object
     * @return ObjectType
     */
    public function generate(ObjectType $object): ObjectType
    {
        return $object;
    }
}