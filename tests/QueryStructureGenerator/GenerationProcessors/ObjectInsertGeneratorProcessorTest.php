<?php

namespace SBuilder\Tests\QueryStructureGenerator\GenerationProcessors;

use GqlDatetime\DateTimeTypes;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\GenerationProcessors\ObjectInsertGeneratorProcessor;
use SBuilder\Tests\Mocks\AliasGenerationServiceMock;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class ObjectInsertGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGeneration() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                [
                    'type' => Type::listOf(new InputObjectType([
                        'name' => "test_object_type",
                        'fields' => [
                            'id' => [
                                'type' => Type::int(),
                                'description' => "Значение для вставки для сущности 'User' поля 'id'",
                            ],
                            'email' => [
                                'type' => Type::string(),
                                'description' => "Значение для вставки для сущности 'User' поля 'email'",
                            ],
                        ],
                        'description' => "Тип объекта для вставки значения для сущности: 'User'",
                    ])),
                    'description' => "Массив сущностей для вставки",
                ]
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => new ObjectType([
                            'name' => 'o_type',
                            'fields' => [
                                'id' => Type::int(),
                            ],
                        ])
                    ]
                ]),
                "test",
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'date' => DateTimeTypes::dateTime(),
                    ]
                ]),
                "test",
                [
                    'type' => Type::listOf(new InputObjectType([
                        'name' => "test_object_type",
                        'fields' => [
                            'id' => [
                                'type' => Type::int(),
                                'description' => "Значение для вставки для сущности 'User' поля 'id'",
                            ],
                            'date' => [
                                'type' => DateTimeTypes::dateTime(),
                                'description' => "Значение для вставки для сущности 'User' поля 'date'",
                            ],
                        ],
                        'description' => "Тип объекта для вставки значения для сущности: 'User'",
                    ])),
                    'description' => "Массив сущностей для вставки",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGeneration
     * @param ObjectType $object
     * @param string $namePrefix
     * @param array|null $result
     */
    public function testForGeneration(ObjectType $object, string $namePrefix, ?array $result) {
        $instance = new ObjectInsertGeneratorProcessor(
            new GraphQlRootTypeGetterMock(),
            new AliasGenerationServiceMock()
        );

        $this->assertEquals($result, $instance->generate($object, $namePrefix));
    }
}