<?php

namespace SBuilder\Tests\QueryStructureGenerator\GenerationProcessors;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\GenerationProcessors\WhereOrHavingQueryGeneratorProcessor;
use SBuilder\QueryStructureGenerator\WhereParametersGenerator\Types\WhereParametersGeneratorInterface;

class WhereOrHavingQueryGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGeneration() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new WhereParametersGeneratorMock(),
                "where",
                null
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new WhereParametersGeneratorMock(['id' => Type::int()]),
                "where",
                [
                    'type' => new InputObjectType([
                        'name' => "test_query_where_object",
                        'fields' => ['id' => Type::int()],
                        'description' => "User: Фильтр 'where' для листинга сущностей.",
                    ]),
                    'description' => "User: Фильтр 'where' для листинга сущностей.",
                ]
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new WhereParametersGeneratorMock(['id' => Type::int()]),
                "having",
                [
                    'type' => new InputObjectType([
                        'name' => "test_query_having_object",
                        'fields' => ['id' => Type::int()],
                        'description' => "User: Фильтр 'having' для листинга сущностей.",
                    ]),
                    'description' => "User: Фильтр 'having' для листинга сущностей.",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGeneration
     * @param ObjectType $object
     * @param string $namePrefix
     * @param WhereParametersGeneratorInterface $generator
     * @param string $name
     * @param array|null $result
     */
    public function testForGeneration(
        ObjectType $object,
        string $namePrefix,
        WhereParametersGeneratorInterface $generator,
        string $name,
        ?array $result
    ) {
        $instance = new WhereOrHavingQueryGeneratorProcessor(
            $generator,
            $name
        );

        $this->assertEquals($result, $instance->generate($object, $namePrefix));
    }
}