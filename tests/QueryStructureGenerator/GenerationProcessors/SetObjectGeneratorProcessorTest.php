<?php

namespace SBuilder\Tests\QueryStructureGenerator\GenerationProcessors;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQlNullableField\Nullable;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\GenerationProcessors\SetObjectGeneratorProcessor;
use SBuilder\Tests\Mocks\AliasGenerationServiceMock;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class SetObjectGeneratorProcessorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGeneration() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new GraphQlRootTypeGetterMock(),
                [
                    'type' => new InputObjectType([
                        'name' => "test_set_type",
                        'fields' => [
                            'id' => [
                                'type' => Type::int(),
                                'description' => "Значение для обновления для сущности 'User' поля 'id'",
                            ],
                            'email' => [
                                'type' => Type::string(),
                                'description' => "Значение для обновления для сущности 'User' поля 'email'",
                            ],
                        ],
                        'description' => "Тип объекта для обновления значения для сущности: 'User'",
                    ]),
                    'description' => "Сущность для обновления",
                ]
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => new ObjectType([
                            'name' => 'o_type',
                            'fields' => [
                                'id' => Type::int(),
                            ],
                        ])
                    ]
                ]),
                "test",
                new GraphQlRootTypeGetterMock(),
                null,
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new GraphQlRootTypeGetterMock(false, false, true),
                [
                    'type' => new InputObjectType([
                        'name' => "test_set_type",
                        'fields' => [
                            'id' => [
                                'type' => Nullable::create(Type::int()),
                                'description' => "Значение для обновления для сущности 'User' поля 'id'",
                            ],
                            'email' => [
                                'type' => Nullable::create(Type::string()),
                                'description' => "Значение для обновления для сущности 'User' поля 'email'",
                            ],
                        ],
                        'description' => "Тип объекта для обновления значения для сущности: 'User'",
                    ]),
                    'description' => "Сущность для обновления",
                ]
            ],
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string()
                    ]
                ]),
                "test",
                new GraphQlRootTypeGetterMock(false, true, true),
                [
                    'type' => new InputObjectType([
                        'name' => "test_set_type",
                        'fields' => [
                            'id' => [
                                'type' => Type::listOf(Nullable::create(Type::int())),
                                'description' => "Значение для обновления для сущности 'User' поля 'id'",
                            ],
                            'email' => [
                                'type' => Type::listOf(Nullable::create(Type::string())),
                                'description' => "Значение для обновления для сущности 'User' поля 'email'",
                            ],
                        ],
                        'description' => "Тип объекта для обновления значения для сущности: 'User'",
                    ]),
                    'description' => "Сущность для обновления",
                ]
            ],
        ];
    }

    /**
     * Тестирование метода generate
     *
     * @dataProvider dataForGeneration
     * @param ObjectType $object
     * @param string $namePrefix
     * @param GraphQlRootTypeGetterInterface $typeGetter
     * @param array|null $result
     */
    public function testForGeneration(
        ObjectType $object,
        string $namePrefix,
        GraphQlRootTypeGetterInterface $typeGetter,
        ?array $result
    ) {
        $instance = new SetObjectGeneratorProcessor(
            $typeGetter,
            new AliasGenerationServiceMock()
        );

        $this->assertEquals($result, $instance->generate($object, $namePrefix));
    }
}