<?php

namespace SBuilder\Tests\QueryStructureGenerator\PaginationParametersGenerator;

use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\PaginationParametersGenerator\PaginationParametersGenerator;

class PaginationParametersGeneratorTest extends TestCase
{
    /**
     * Тестирование метода generateLimit
     */
    public function testGenerateLimit() {
        $instance = new PaginationParametersGenerator();

        $this->assertEquals([
            'type' => Type::int(),
            'defaultValue' => 30,
            'description' => "Параметр пагинации: Количество элементов в выдаче.",
        ], $instance->generateLimit());
    }

    /**
     * Тестирование метода generateOffset
     */
    public function testGenerateOffset() {
        $instance = new PaginationParametersGenerator();

        $this->assertEquals([
            'type' => Type::int(),
            'defaultValue' => 0,
            'description' => "Параметр пагинации: Смещение для элементов в выдаче.",
        ], $instance->generateOffset());
    }
}