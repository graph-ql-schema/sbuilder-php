<?php

namespace SBuilder\Tests\QueryStructureGenerator\OrderParametersGenerator;

use GraphQL\Type\Definition\EnumType;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\OrderParametersGenerator\OrderParametersGenerator;
use SBuilder\Tests\Mocks\GraphQlRootTypeGetterMock;

class OrderParametersGeneratorTest extends TestCase
{
    /**
     * Набор данных для тестирования метода generate
     *
     * @return array
     */
    public function dataForGenerate() {
        return [
            [
                new ObjectType([
                    "name" => "User",
                    'description' => 'Our blog visitor',
                    'fields' => [
                        'id' => Type::int(),
                        'email' => Type::string(),
                        'testUnion' => new UnionType([
                            'name' => 'testUnionType',
                            'types' => [
                                new ObjectType([
                                    'name' => 'unionObjectType1',
                                    'fields' => [
                                        'firstName' => Type::string()
                                    ]
                                ]),
                                new ObjectType([
                                    'name' => 'unionObjectType2',
                                    'fields' => [
                                        'lastName' => Type::string()
                                    ]
                                ])
                            ]
                        ])
                    ]
                ]),
                "user_prefix",
                [
                    'type' => Type::listOf(new InputObjectType([
                        'name' => "user_prefix_order_parameters_object",
                        'fields' => [
                            "by" => [
                                'type' => new EnumType([
                                    'name' => "user_prefix_order_parameters_object_by",
                                    'description' => "Варианты выбора полей для сортировки",
                                    'values' => [
                                        "id" => [
                                            'value' => "id",
                                            'description' => "Значение id"
                                        ],
                                        "email" => [
                                            'value' => "email",
                                            'description' => "Значение email"
                                        ],
                                    ],
                                ]),
                                'description' => "Варианты выбора полей для сортировки",
                            ],
                            "direction" => [
                                'type' => new EnumType([
                                    'name' => "user_prefix_order_parameters_object_order",
                                    'description' => "Варианты сортировки",
                                    'values' => [
                                        "asc" => [
                                            'value' => "asc",
                                            'description' => "Сортировка по возрастанию"
                                        ],
                                        "desc" => [
                                            'value' => "desc",
                                            'description' => "Сортировка по убыванию"
                                        ],
                                    ],
                                ]),
                            ],
                            "priority" => [
                                'type' => Type::int(),
                                'defaultValue' => 500,
                                'description' => "Приоритет применения сортировки. Должен быть больше 0.",
                            ],
                        ],
                        "description" => "Сортировка по переданным параметрам",
                    ])),
                    "description" => "Сортировка по переданным параметрам",
                ],
            ],
        ];
    }

    /**
     * Тестирование метода
     *
     * @dataProvider dataForGenerate
     * @param ObjectType $baseObject
     * @param string $prefix
     * @param array $result
     */
    public function testGenerate(ObjectType $baseObject, string $prefix, array $result) {
        $instance = new OrderParametersGenerator(new GraphQlRootTypeGetterMock(), null);

        $this->assertEquals($result, $instance->generate($baseObject, $prefix));
    }
}
