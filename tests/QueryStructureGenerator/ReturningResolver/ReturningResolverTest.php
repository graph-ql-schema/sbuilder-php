<?php

namespace SBuilder\Tests\QueryStructureGenerator\ReturningResolver;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use PHPUnit\Framework\TestCase;
use SBuilder\QueryStructureGenerator\ReturningResolver\ReturningResolver;

class ReturningResolverTest extends TestCase
{
    /**
     * Тестирование метода parse на корректных данных
     *
     * @return array
     */
    public function dataForParse() {
        return [
            [null, null],
            [[], null],
            [["returning" => [["id" => 1]]], [["id" => 1]]],
        ];
    }

    /**
     * Тестирование метода parse на корректных данных
     *
     * @dataProvider dataForParse
     * @param $data
     * @param $result
     */
    public function testForParse($data, $result) {
        $instance = new ReturningResolver(
            new GraphQlSqlConverterMock(),
            new ObjectType([
                "name" => "User",
                'description' => 'Our blog visitor',
                'fields' => [
                    'id' => Type::int(),
                    'email' => Type::string()
                ]
            ])
        );

        $this->assertEquals($result, $instance->parse($data));
    }

    /**
     * Тестирование метода parse на не корректных данных
     *
     * @return array
     */
    public function dataForParseInvalid() {
        return [
            [123],
            ["test"],
            [false],
        ];
    }

    /**
     * Тестирование метода parse на корректных данных
     *
     * @dataProvider dataForParseInvalid
     * @param $data
     * @expectedException \LogicException
     */
    public function testForParseInvalid($data) {
        $instance = new ReturningResolver(
            new GraphQlSqlConverterMock(),
            new ObjectType([
                "name" => "User",
                'description' => 'Our blog visitor',
                'fields' => [
                    'id' => Type::int(),
                    'email' => Type::string()
                ]
            ])
        );

        $instance->parse($data);
    }
}