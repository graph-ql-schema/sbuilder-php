<?php

namespace SBuilder\Tests\QueryStructureGenerator\ReturningResolver;

use GqlSqlConverter\Converter\GraphQlSqlConverterInterface;
use GqlSqlConverter\Exceptions\ConvertationException;
use GraphQL\Type\Definition\ObjectType;

/**
 * Подставка для тестирования
 */
class GraphQlSqlConverterMock implements GraphQlSqlConverterInterface
{
    /** @var bool */
    private $isError;

    /**
     * GraphQlSqlConverterMock constructor.
     *
     * @param bool $isError
     */
    public function __construct(bool $isError = false)
    {
        $this->isError = $isError;
    }

    /**
     * Конвертация в базовый тип, например в строку или число
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return mixed
     * @throws ConvertationException
     */
    public function toBaseType(ObjectType $object, string $field, $value)
    {
        if ($this->isError) {
            throw new ConvertationException("Test");
        }

        return $value;
    }

    /**
     * Конвертация в SQL like значение
     *
     * @param ObjectType $object
     * @param string $field
     * @param $value
     *
     * @return string
     * @throws ConvertationException
     */
    public function toSQLValue(ObjectType $object, string $field, $value): string
    {
        if ($this->isError) {
            throw new ConvertationException("Test");
        }

        return $value;
    }
}