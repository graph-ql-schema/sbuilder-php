<?php

namespace SBuilder\Tests\Mocks;

use SBuilder\QueryStructureGenerator\AliasGenerationService\AliasGenerationServiceInterface;

/**
 * Подставка для тестирования
 */
class AliasGenerationServiceMock implements AliasGenerationServiceInterface
{
    /**
     * Alias generation.
     *
     * @param string $source
     * @return string
     */
    public function generate(string $source): string
    {
        return strtolower($source);
    }
}