<?php

namespace SBuilder\Tests\Mocks;

use GraphQL\Language\AST\FragmentDefinitionNode;
use GraphQL\Language\AST\NameNode;
use GraphQL\Language\AST\NodeList;
use GraphQL\Language\AST\SelectionSetNode;

/**
 * Фабрика тестовых сущностей FragmentDefinitionNode
 */
class FragmentDefinitionNodeFactory
{
    /**
     * Для тестирования парсинга параметров запроса необходимо эмулировать структуру
     * входящего запроса. Для этих целей необходимо генерировать сущности FieldNode.
     * По ним в библиотеке строится список запрошенных полей.
     *
     * @param string $name
     * @param array|null $selections
     * @return FragmentDefinitionNode
     */
    public static function make(string $name, array $selections = null): FragmentDefinitionNode {
        $testFieldNode = new FragmentDefinitionNode([]);
        $testFieldNode->name = new NameNode([]);
        $testFieldNode->name->value = $name;

        $node = null;
        if (null !== $selections) {
            $node = new SelectionSetNode([]);
            $node->selections = NodeList::create($selections);
        }

        $testFieldNode->selectionSet = $node;

        return $testFieldNode;
    }
}