<?php

namespace SBuilder\Tests\Mocks;

use GraphQL\Language\AST\FragmentSpreadNode;
use GraphQL\Language\AST\NameNode;

/**
 * Фабрика тестовых сущностей FragmentSpreadNode
 */
class FragmentSpreadNodeFactory
{
    /**
     * Для тестирования парсинга параметров запроса необходимо эмулировать структуру
     * входящего запроса. Для этих целей необходимо генерировать сущности FieldNode.
     * По ним в библиотеке строится список запрошенных полей.
     *
     * @param string $name
     * @return FragmentSpreadNode
     */
    public static function make(string $name): FragmentSpreadNode {
        $testFieldNode = new FragmentSpreadNode([]);
        $testFieldNode->name = new NameNode([]);
        $testFieldNode->name->value = $name;

        return $testFieldNode;
    }
}