<?php

namespace SBuilder\Tests\Mocks;

use GqlRootTypeGetter\GraphQlRootTypeGetterInterface;
use GraphQL\Type\Definition\Type;

/**
 * Подставка для тестирования
 */
class GraphQlRootTypeGetterMock implements GraphQlRootTypeGetterInterface
{
    /** @var bool */
    private $isNotNullResult;

    /** @var bool */
    private $isListResult;

    /** @var bool */
    private $isNullableResult;

    /**
     * GraphQlRootTypeGetterMock constructor.
     *
     * @param bool $isNotNullResult
     * @param bool $isListResult
     * @param bool $isNullableResult
     */
    public function __construct(bool $isNotNullResult = false, bool $isListResult = false, bool $isNullableResult = false)
    {
        $this->isNotNullResult = $isNotNullResult;
        $this->isListResult = $isListResult;
        $this->isNullableResult = $isNullableResult;
    }

    /**
     * Получение корневого типа поля, которое обернут в NotNull или в List
     * @param Type $multipleType
     * @return Type
     */
    public function getRootType(Type $multipleType): Type
    {
        return $multipleType;
    }

    /**
     * Проверяет, что переданный тип является NotNull
     * @param Type $multipleType
     * @return bool
     */
    public function isNotNull(Type $multipleType): bool
    {
        return $this->isNotNullResult;
    }

    /**
     * Проверяет, что переданный тип является List
     * @param Type $multipleType
     * @return bool
     */
    public function isList(Type $multipleType): bool
    {
        return $this->isListResult;
    }

    /**
     * Проверяет, что переданный тип является Nullable типом
     *
     * @param Type $multipleType
     * @return bool
     */
    public function isNullable(Type $multipleType): bool
    {
        return $this->isNullableResult;
    }
}