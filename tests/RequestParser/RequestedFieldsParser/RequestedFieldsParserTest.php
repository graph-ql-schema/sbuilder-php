<?php

namespace SBuilder\Tests\RequestParser\RequestedFieldsParser;

use GraphQL\Type\Definition\FieldDefinition;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
use PHPUnit\Framework\TestCase;
use SBuilder\RequestParser\RequestedFieldsParser\RequestedField;
use SBuilder\RequestParser\RequestedFieldsParser\RequestedFieldsParser;
use SBuilder\RequestParser\Types\ResolveParams;
use SBuilder\Tests\Mocks\FieldNodeFactory;
use SBuilder\Tests\Mocks\FragmentDefinitionNodeFactory;
use SBuilder\Tests\Mocks\FragmentSpreadNodeFactory;

class RequestedFieldsParserTest extends TestCase
{
    /**
     * Test data for parseRequest test.
     *
     * @return array[]
     */
    public function parseRequestDataProvider(): array
    {
        return [
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [],
                        null,
                        null,
                        []
                    )
                ),
                [],
            ],
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [FieldNodeFactory::make("query", [FieldNodeFactory::make("test")])],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [],
                        null,
                        null,
                        []
                    )
                ),
                [new RequestedField("test")],
            ],
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [FieldNodeFactory::make("query", [
                            FieldNodeFactory::make("test", [
                                FieldNodeFactory::make("data")
                            ])
                        ])],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [],
                        null,
                        null,
                        []
                    )
                ),
                [
                    new RequestedField("test", [new RequestedField("data")]),
                ],
            ],
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [FieldNodeFactory::make("query", [
                            FieldNodeFactory::make("test", [
                                FieldNodeFactory::make("data")
                            ]),
                            FieldNodeFactory::make("test-2", [
                                FieldNodeFactory::make("data")
                            ])
                        ])],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [],
                        null,
                        null,
                        []
                    )
                ),
                [
                    new RequestedField("test", [new RequestedField("data")]),
                    new RequestedField("test-2", [new RequestedField("data")]),
                ],
            ],
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [FieldNodeFactory::make("query", [
                            FragmentSpreadNodeFactory::make("test"),
                        ])],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [
                            "test" => FragmentDefinitionNodeFactory::make("test", [
                                FragmentDefinitionNodeFactory::make("data")
                            ]),
                        ],
                        null,
                        null,
                        []
                    )
                ),
                [
                    new RequestedField("test", [new RequestedField("data")]),
                ],
            ],
            [
                // ResolveParams
                new ResolveParams(
                    [], [], null, new ResolveInfo(
                        FieldDefinition::create(['name' => 'test', 'type' => Type::int()]),
                        [FieldNodeFactory::make("query", [
                            FragmentSpreadNodeFactory::make("test"),
                        ])],
                        new ObjectType(['name' => 'test', 'fields' => ['id' => Type::int()]]),
                        [],
                        new Schema([]),
                        [
                            "test" => FragmentDefinitionNodeFactory::make("test"),
                        ],
                        null,
                        null,
                        []
                    )
                ),
                [
                    new RequestedField("test"),
                ],
            ],
        ];
    }

    /**
     * @dataProvider parseRequestDataProvider
     * @param ResolveParams $resolveParams
     * @param array $expect
     */
    public function testParseRequest(ResolveParams $resolveParams, array $expect)
    {
        $instance = new RequestedFieldsParser();
        $this->assertEquals($expect, $instance->parseRequest($resolveParams));
    }
}
