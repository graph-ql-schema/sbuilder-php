<?php

namespace SBuilder\Tests\RequestParser\RequestedFieldsParser;

use PHPUnit\Framework\TestCase;
use SBuilder\RequestParser\RequestedFieldsParser\RequestedField;

class RequestFieldTest extends TestCase
{
    /**
     * Test data for getFieldName test.
     *
     * @return array[]
     */
    public function getFieldNameDataProvider(): array
    {
        return [
            ['test', [], 'test']
        ];
    }

    /**
     * @dataProvider getFieldNameDataProvider
     * @param string $fieldName
     * @param array $subFields
     * @param string $expect
     */
    public function testGetFieldName(string $fieldName, array $subFields, string $expect)
    {
        $instance = new RequestedField($fieldName, $subFields);
        $this->assertEquals($expect, $instance->getFieldName());
    }

    /**
     * Test data for hasSubFields test.
     *
     * @return array[]
     */
    public function hasSubFieldsDataProvider(): array
    {
        return [
            // Test for empty subFields collection.
            ['empty', [], false],
            // Test for subFields collection with some data in it.
            ['some', [new RequestedField('empty', [])], true]
        ];
    }

    /**
     * @dataProvider hasSubFieldsDataProvider
     * @param string $fieldName
     * @param array $subFields
     * @param string $expect
     */
    public function testHasSubFields(string $fieldName, array $subFields, string $expect)
    {
        $instance = new RequestedField($fieldName, $subFields);
        $this->assertEquals($expect, $instance->hasSubFields());
    }

    /**
     * Test data for getSubFields test.
     *
     * @return array[]
     */
    public function getSubFieldsDataProvider(): array
    {
        return [
            // Test with empty subFields collection.
            ['empty', [], []],
            // Test with subFields collection that has some data in it.
            ['some', [new RequestedField('empty', [])], [new RequestedField('empty', [])]]
        ];
    }

    /**
     * @dataProvider getSubfieldsDataProvider
     * @param string $fieldName
     * @param array $subFields
     * @param array $expect
     */
    public function testGetSubFields(string $fieldName, array $subFields, array $expect)
    {
        $instance = new RequestedField($fieldName, $subFields);
        $this->assertEquals($expect, $instance->getSubFields());
    }
}
