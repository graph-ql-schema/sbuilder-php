<?php

/**
 * Данный файл описывает примерную схему реализации GraphQL API с использованием
 * библиотеки SBuilder. По сути тут приведен пример того, как создать схему из
 * какой либо сущности (в данном случае User из файла user.php), а также резолвить
 * запросы (TestResolver.php). Данный пример не является обязательной схемой работы,
 * вы можете модифицировать его как пожелаете. Также пример не гарантирует максимальную
 * производительность.
 *
 * Для запуска тестового кода необходимо в консоли перейти в эту директорию и выполнить
 * команду:
 *
 * php -S localhost:8080
 *
 * Данная команда запустит тестовый PHP сервер и вам станет доступен GraphQL endpoint
 * по адресу: localhost:8080.
 *
 * После запуска сервера необходимо подключить данный endpoint к любому GraphQL клиенту
 * (Например: https://chrome.google.com/webstore/detail/altair-graphql-client/flnheeellpciglgpaodhkhmapeljopja)
 * и провести интроспекцию схемы. По сути это подтянет всю документацию по текущей схеме
 * GraphQL.
 */

use GraphQL\GraphQL;
use SBuilder\Constants\SBuilderConstants;
use SBuilder\GraphQlSchemaBuilder;

require "../vendor/autoload.php";
require "TestResolver.php";

$schemaBuilder = GraphQlSchemaBuilder::make();

$userObject = require "user.php";

$schemaBuilder->registerEntity($userObject, [
    SBuilderConstants::LIST_QUERY => new TestResolver(),
    SBuilderConstants::AGGREGATE_QUERY => new TestResolver(),
    SBuilderConstants::INSERT_MUTATION => new TestResolver(),
    SBuilderConstants::UPDATE_MUTATION => new TestResolver(),
    SBuilderConstants::DELETE_MUTATION => new TestResolver(),
]);

$schema = $schemaBuilder->buildSchema();

$rawInput = file_get_contents('php://input');
$input = json_decode($rawInput, true);
$query = $input['query'];
$variableValues = isset($input['variables']) ? $input['variables'] : null;

try {
    $rootValue = ['prefix' => 'You said: '];
    $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);
    $output = $result->toArray();
} catch (Exception $e) {
    $output = [
        'errors' => [
            [
                'message' => $e->getMessage()
            ]
        ]
    ];
}

header('Content-Type: application/json');
echo json_encode($output);