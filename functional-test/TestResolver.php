<?php

use GraphQL\Type\Definition\ObjectType;
use SBuilder\RequestParser\Types\ResolveParams;
use SBuilder\RequestResolver\ResolverInterface;

class TestResolver implements ResolverInterface
{
    /**
     * Резолвинг запроса GraphQL.
     *
     * На выходе должен получиться либо массив с реузльтатами работы, либо
     * объект, готовый к сериализации.
     *
     * @param ResolveParams $originalParams
     * @param ObjectType $graphQlObject
     * @param array $fields
     *
     * @return array|JsonSerializable|null
     * @throws Throwable
     */
    public function resolveQuery(ResolveParams $originalParams, ObjectType $graphQlObject, array $fields)
    {
        return null;
    }
}