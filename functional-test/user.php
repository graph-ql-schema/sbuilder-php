<?php

use GqlDatetime\DateTimeTypes;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\UnionType;

return new ObjectType([
    'name' => 'User',
    'description' => 'User test entity',
    'fields' => [
        'id' => Type::id(),
        'name' => Type::string(),
        'birthDate' => DateTimeTypes::dateTime(),
        'balance' => Type::float(),
        'testUnion' => new UnionType([
            'name' => 'testUnionType',
            'types' => [
                new ObjectType([
                    'name' => 'unionObjectType1',
                    'fields' => [
                        'firstName' => Type::string()
                    ]
                ]),
                new ObjectType([
                    'name' => 'unionObjectType2',
                    'fields' => [
                        'lastName' => Type::string()
                    ]
                ])
            ]
        ])
    ]
]);
